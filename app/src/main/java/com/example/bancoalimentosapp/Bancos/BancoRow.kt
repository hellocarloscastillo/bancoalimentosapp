package com.example.bancoalimentosapp.Bancos

import java.io.Serializable

data class BancoRow(
    var id_bancos_alimentos: Int,
    var nombre: String,
    var id_usuario: String,
    var estado:Boolean
): Serializable
