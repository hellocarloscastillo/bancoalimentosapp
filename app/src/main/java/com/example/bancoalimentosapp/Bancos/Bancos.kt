package com.example.bancoalimentosapp.Bancos

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.BancosFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Banco
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Bancos : Fragment() {

    companion object {
        fun newInstance() = Bancos()
    }

    private lateinit var viewModel: BancosViewModel
    private var _binding: BancosFragmentBinding? = null
    lateinit var dialog: Dialog;

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var usuario: UsuarioSesion

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = BancosFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = BancosViewModel(activity)
        val btnAddEMployee: ImageButton = binding.btnAddBank

        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)

        //init list recycler view
        val recyclerListBanco = activity?.findViewById<RecyclerView>(R.id.listBancos)
        recyclerListBanco?.layoutManager = LinearLayoutManager(activity)


        viewModel.bancoService.listarBnacosPorUsuario(usuario.id_usuario).enqueue(
            object : Callback<List<Banco>> {

                override fun onResponse(call: Call<List<Banco>>?, response: Response<List<Banco>>?) {
                    if(response!!.isSuccessful){
                        if(response.isSuccessful){
                            val listaBancosRow: MutableList<BancoRow> = mutableListOf()
                            if(response!!.body()!!.size > 0){
                                response!!.body()!!.forEach {
                                    if(it.estado ){
                                        listaBancosRow.add(BancoRow(it.id_bancos_alimentos, it.nombre, it.id_usuario.toString(), it.estado ))
                                    }
                                }
                                val adapter = ListBancoAdapter(listaBancosRow, context!!, this@Bancos)
                                recyclerListBanco!!.adapter = adapter
                            }else{
                                Toast.makeText(context, "NO hay empleados registrados", Toast.LENGTH_LONG).show()
                            }
                        }else{
                            Toast.makeText(context, "NO se pudieron cargar los empleados", Toast.LENGTH_LONG).show()
                        }
                    }else{
                        Toast.makeText(context, "NO se pudieron cargar los empleados", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<Banco>>?, t: Throwable?) {
                    Toast.makeText(context, "NO se pudieron cargar los empleados", Toast.LENGTH_LONG).show()
                }
            }
        )


        btnAddEMployee.setOnClickListener {
            println("hola ? ")
            findNavController().navigate(R.id.registroBanco )
        }
    }

}