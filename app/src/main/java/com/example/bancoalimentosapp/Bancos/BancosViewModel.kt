package com.example.bancoalimentosapp.Bancos

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BancosViewModel (val activity: Activity?) {
    public val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)


}