package com.example.bancoalimentosapp.Bancos

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.os.Bundle
import com.example.bancoalimentosapp.models.Banco
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ListBancoAdapter (private val mList: MutableList<BancoRow>, private val contex: Context, private val fragment: Fragment): RecyclerView.Adapter<ListBancoAdapter.ViewHolder>() {

    private val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    lateinit var dialog: Dialog;

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_list_banco, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val ItemViewModel = mList[position]
        dialog = Dialog(contex)
        // sets the image to the imageview from our itemHolder class
        holder.idBank.text = ItemViewModel.id_bancos_alimentos.toString()
        holder.namebak.text = ItemViewModel.nombre


        holder.btnOption.setOnClickListener { //creating a popup menu

            val popup = PopupMenu(contex, holder.btnOption)
            //inflating menu from xml resource
            popup.inflate(R.menu.option_row_bank)
            //adding click listener
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem ): Boolean {
                    return when (item.getItemId()) {
                        R.id.editBankOptionRow ->                 //handle menu1 click
                        {
                            var bancoUpdate = Bundle()
                            bancoUpdate.putSerializable("bancoUpdate", ItemViewModel )
                            fragment.findNavController().navigate(R.id.registroBanco, bancoUpdate )
                            true
                        }
                        R.id.deleteBankOptionRow ->                         //handle menu2 click
                        {
                            openOptionRegister(ItemViewModel, position)
                            true
                        }



                        else -> true
                    }
                }
            })
            //displaying the popup
            popup.show()
        }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val idBank:TextView = itemView.findViewById<TextView>(R.id.idBanco)
        val namebak:TextView = itemView.findViewById(R.id.namebank)
        val btnOption: ImageButton = itemView.findViewById<ImageButton>(R.id.btnOptioniTemBank)
    }

    private fun openOptionRegister(ItemViewModel:BancoRow, position: Int){
        dialog.setContentView(R.layout.mensaje_confirmacion)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        var btnYes = dialog.findViewById<Button>(R.id.btnYesDialogConfirm)
        var btnNo = dialog.findViewById<Button>(R.id.btnNoDialogConfirm)
        var text = dialog.findViewById<TextView>(R.id.textDialogCOnfirm)
        text.text = "Seguro que sea eliminar el banco ${ItemViewModel.nombre}"
        btnYes.setOnClickListener {
            bancoService.eliminarBanco(ItemViewModel).enqueue(
                object : Callback<Any>{
                    override fun onResponse(
                        call: Call<Any>,
                        response: Response<Any>
                    ) {
                        if(response.isSuccessful){
                            fragment.findNavController().navigate(R.id.bancos)
                            dialog.dismiss()
                            Toast.makeText(contex, "Banco eliminado", Toast.LENGTH_LONG).show()
                        }else{
                            dialog.dismiss()
                            Toast.makeText(contex, "No se pudo eliminar el banco, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<Any>, t: Throwable) {
                        mList.remove(ItemViewModel)
                        notifyItemRemoved(position)
                        dialog.dismiss()
                        Toast.makeText(contex, "Banco eliminado", Toast.LENGTH_LONG).show()
                    }
                }
            )

        }

        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}