package com.example.bancoalimentosapp.Bancos.registroBanco

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.Bancos.BancoRow
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.RegistroBancoFragmentBinding
import com.example.bancoalimentosapp.models.Banco

class RegistroBanco : Fragment() {

    companion object {
        fun newInstance() = RegistroBanco()
    }

    private lateinit var viewModel: RegistroBancoViewModel
    private var _binding: RegistroBancoFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RegistroBancoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = RegistroBancoViewModel(activity, this)

        var bancoAux = arguments?.getSerializable("bancoUpdate")

        if(bancoAux == null){
            this.ifNew()
        }else{
            ifUpdate(bancoAux as BancoRow)
        }
    }

    fun ifUpdate(bancoAux:BancoRow){
        val nombreBanco: TextView = binding.txtNombreBanco
        nombreBanco.text = bancoAux.nombre
        val btnRegister: Button = binding.btnRegistrarBanco
        btnRegister.text = "Actualizar"
        binding.textView4.text = ""
        binding.textView5.text = ""
        val btnCancel: Button = binding.btnCancelar

        btnRegister.setOnClickListener {
            bancoAux.nombre = nombreBanco.text.toString()
            viewModel.updateBanco(bancoAux)
        }

        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.bancos )
        }
    }

    fun ifNew(){
        val nombreBanco: TextView = binding.txtNombreBanco

        val btnRegister: Button = binding.btnRegistrarBanco
        val btnCancel: Button = binding.btnCancelar

        btnRegister.setOnClickListener {
            viewModel.createBanco(nombreBanco.text.toString())
        }

        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.loginFragment )
        }
    }

}