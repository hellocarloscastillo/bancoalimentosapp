package com.example.bancoalimentosapp.Bancos.registroBanco

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.Bancos.BancoRow
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegistroBancoViewModel(val activity: Activity?, val fragment: Fragment)  {

    private val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    fun createBanco(nombre:String){
        val newBanco = NewBanco(nombre, usuarioSession.id_usuario)
        bancoService.guardarBanco(newBanco).enqueue(
            object : Callback<Banco> {

                override fun onResponse(
                    call: Call<Banco>?,
                    response: Response<Banco>?
                ) {
                    if(response!!.isSuccessful){
                        Toast.makeText(fragment.context, "Banco Registrado", Toast.LENGTH_LONG).show()
                        fragment.findNavController().navigate(R.id.empleados  )
                    }
                    else{
                        Toast.makeText(fragment.context, "No se pudo registrar el banco por favor intenta mas tarde o contacta con el administrador", Toast.LENGTH_LONG).show()
                    }

                }

                override fun onFailure(call: Call<Banco>?, t: Throwable?) {
                    Toast.makeText(fragment.context, "Ocurrio Un problema con el registro: ${t?.toString()}", Toast.LENGTH_LONG).show()
                }
            })
    }

    fun updateBanco(bancoUpdt:BancoRow){
        bancoService.actualizarBanco(bancoUpdt).enqueue(
            object : Callback<Banco> {

                override fun onResponse(
                    call: Call<Banco>?,
                    response: Response<Banco>?
                ) {
                    if(response!!.isSuccessful){
                        Toast.makeText(fragment.context, "Banco actualizado", Toast.LENGTH_LONG).show()
                        fragment.findNavController().navigate(R.id.bancos  )
                    }
                    else{
                        Toast.makeText(fragment.context, "No se pudo actualizar el banco por favor intenta mas tarde o contacta con el administrador", Toast.LENGTH_LONG).show()
                    }

                }

                override fun onFailure(call: Call<Banco>?, t: Throwable?) {
                    Toast.makeText(fragment.context, "Ocurrio Un problema con la actualizacion: ${t?.toString()}", Toast.LENGTH_LONG).show()
                }
            })
    }
}