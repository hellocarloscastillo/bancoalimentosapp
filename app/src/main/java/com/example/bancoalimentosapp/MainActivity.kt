package com.example.bancoalimentosapp

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.bancoalimentosapp.databinding.ActivityMainBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    lateinit var sharedPreferences: SharedPreferences
    lateinit var usuarioSession : UsuarioSesion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        sharedPreferences = getPreferences(Context.MODE_PRIVATE) ?: return

        usuarioSession = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)

        println("usuario session de la shit : " + usuarioSession.toString())

        /*binding.appBarMain.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }*/
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = changeMenuByUsr()

        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            changeOptionMenuByUsr() , drawerLayout
        )


        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 2){
            navView.setNavigationItemSelectedListener {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START)
                } else {
                    drawerLayout.openDrawer(GravityCompat.START)
                }
                when(it.itemId) {

                    R.id.empleados -> {
                        navController.navigate(R.id.empleados)
                        true
                    }
                    R.id.vehiculos -> {
                        navController.navigate(R.id.vehiculos)
                        true
                    }
                    R.id.bancos -> {
                        navController.navigate(R.id.bancos)
                        true
                    }
                    R.id.sobrasDisponibles -> {
                        navController.navigate(R.id.sobrasDisponibles)
                        true
                    }
                    R.id.pedidosBanco -> {
                        navController.navigate(R.id.pedidosBanco)
                        true
                    }

                    R.id.loginFragment -> {
                        var appbar = findViewById(R.id.appbarlayout) as AppBarLayout
                        appbar.visibility = View.INVISIBLE
                        appbar.getLayoutParams().height= 0;
                        var usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)
                        println("usuario de la monda : ${usuario.correo}")
                        sharedPreferences.edit().remove("usuario").commit()
                        this.finishAffinity();
                        true
                    }

                    else ->{
                        true
                    }

                }
            }
        }
        if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 1) {

            navView.setNavigationItemSelectedListener {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START)
                } else {
                    drawerLayout.openDrawer(GravityCompat.START)
                }
                when(it.itemId) {

                    R.id.restaurantes -> {
                        navController.navigate(R.id.listaRestaurante)
                        true
                    }

                    R.id.sobras -> {
                        navController.navigate(R.id.sobrasRestaurante)
                        true
                    }

                    R.id.pedidosRest -> {
                        navController.navigate(R.id.pedidosRestaurante)
                        true
                    }


                    R.id.loginFragment -> {
                        var appbar = findViewById(R.id.appbarlayout) as AppBarLayout
                        appbar.visibility = View.INVISIBLE
                        appbar.getLayoutParams().height= 0;
                        var usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)
                        println("usuario de la monda : ${usuario.correo}")
                        sharedPreferences.edit().remove("usuario").commit()
                        this.finishAffinity();
                        true
                    }

                    else ->{
                        true
                    }

                }
            }
        }




    }

    fun changeMenuByUsr():NavigationView{
        val nv: NavigationView = binding.navView
        if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 1){
            nv.menu.clear()
            nv.inflateMenu(R.menu.menu_restaurante)
        }
        if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 2){
            nv.menu.clear()
            nv.inflateMenu(R.menu.activity_main_drawer)
        }

        return nv

    }

    fun changeOptionMenuByUsr(): Set<Int> {
        if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 1){
            return setOf(
                R.id.listaRestaurante, R.id.pedidosRest, R.id.sobrasRestaurante
            )
        }
        if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 2){
            return setOf(
                R.id.empleados, R.id.vehiculos, R.id.bancos, R.id.pedidosBanco, R.id.sobrasDisponibles
            )
        }
        return setOf(
        )

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)


        if(usuarioSession.id_usuario > 0 ){
            Snackbar.make(binding.root, "Bienvenido de nuevo ${usuarioSession.nombre}", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

            val toast = Toast(this)
            var view: ImageView = ImageView(this);
            view.setImageResource(R.mipmap.icon_ba_uno_foreground);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.view = view;
            toast.show();


            findNavController(R.id.nav_host_fragment_content_main).popBackStack()
            if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 1){

                findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.listaRestaurante)
            }
            if(usuarioSession.id_usuario != 0 && usuarioSession.tipoUsuario.id_tipo_usuario == 2){

                findNavController(R.id.nav_host_fragment_content_main).navigate(R.id.empleados)
            }


        }
        return true
    }

    override fun onSupportNavigateUp(): Boolean {

        val navController = findNavController(R.id.nav_host_fragment_content_main)

        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}