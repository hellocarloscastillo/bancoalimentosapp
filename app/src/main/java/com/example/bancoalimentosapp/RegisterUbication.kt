package com.example.bancoalimentosapp

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.bancoalimentosapp.databinding.FragmentRegisterUbicationBinding
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.utils.Settings

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegisterUbication : Fragment() {


    companion object {
        val PERMISSION_CODE_UBICATION = 101
    }

    private var _binding: FragmentRegisterUbicationBinding? = null
    private val binding get() = _binding!!
    private var currentLocation: Location? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var restauranteAux: Restaurante
    var idBanco:Int = -1
    var idRestaurante:Int = -1

    val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterUbicationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        idBanco = requireArguments().getInt("idBanco", -1)
        idRestaurante = requireArguments().getInt("idRestaurante", -1)
        val btnRegistrarUbicacion: Button = binding.btnRegisterUbication
        var txtNombreUbicacion: TextView = binding.txtNombreUbicacion
        var txtUbicGeo: TextView = binding.txtUbicGeo

        fusedLocationProviderClient =  LocationServices.getFusedLocationProviderClient(requireActivity())


        if(idBanco > 0){
            bancoService.obtenerBancoPorId(idBanco).enqueue(
                object : Callback<Banco> {
                    override fun onResponse(call: Call<Banco>, response: Response<Banco>) {
                        if(response!!.isSuccessful){
                            val bancoAux: Banco = response.body() as Banco
                            txtNombreUbicacion.text = bancoAux.nombre
                            //val ubicacion = bancoAux.geolocalizacion.split(";")
                            //ingresarPuntoEnMapa(ubicacion, bancoAux.nombre)
                        }else{
                            Toast.makeText(activity, "NO se pudo cargar la informacion del banco, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<Banco>, t: Throwable) {
                        Toast.makeText(activity, "NO se pudo cargar la informacion del banco, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )
        }else if(idRestaurante > 0){
            restauranteService.obtenerRestaurantePorId(idRestaurante).enqueue(
                object :Callback<Restaurante> {
                    override fun onResponse(
                        call: Call<Restaurante>,
                        response: Response<Restaurante>
                    ) {
                        if(response!!.isSuccessful){
                            restauranteAux = response.body() as Restaurante
                            txtNombreUbicacion.text = restauranteAux.nombre
                            val ubicacion = restauranteAux.geolocalizacion.split(";")
                            if(!ubicacion[0].equals("-1")){
                                txtUbicGeo.text = restauranteAux.geolocalizacion
                                ingresarPuntoEnMapa(ubicacion, restauranteAux.nombre)
                            }else{
                                txtUbicGeo.text = "No registrado"
                                ingresarPuntoEnMapa(listOf("41.889941", "12.492306"), "???")
                            }

                        }else{
                            Toast.makeText(activity, "NO se pudo cargar la informacion del restaurante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<Restaurante>, t: Throwable) {
                        Toast.makeText(activity, "NO se pudo cargar la informacion del restaurante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )
        }

        btnRegistrarUbicacion.setOnClickListener {
            cambiarUbicacionActual(it)
        }

        pedirPermisosUbicacion(view)




    }

    fun ingresarPuntoEnMapa(punto:List<String>, nombre:String){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync {
            val latLng = LatLng(punto[0].toDouble(),punto[1].toDouble())
            val markerOptions = MarkerOptions().position(latLng).title(nombre)
            it.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            it.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14f))
            it.addMarker(markerOptions)
        }
    }

    fun cambiarUbicacionActual(view: View){
        if (view.context.packageManager.hasSystemFeature(
                PackageManager.FEATURE_LOCATION ) && view.context.packageManager.hasSystemFeature(
                PackageManager.FEATURE_LOCATION_NETWORK ) && view.context.packageManager.hasSystemFeature(
                PackageManager.FEATURE_LOCATION_GPS )) {

            if (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_DENIED
            ) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION ), PERMISSION_CODE_UBICATION)
            }else{
                val task = fusedLocationProviderClient.lastLocation
                task.addOnSuccessListener {
                    if (it != null) {
                        currentLocation = it
                        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
                        mapFragment!!.getMapAsync {
                            if(currentLocation!!.latitude != null){

                                ingresarPuntoEnMapa(listOf(currentLocation!!.latitude.toString(), currentLocation!!.longitude.toString()).toList(),restauranteAux.nombre)
                                restauranteAux.geolocalizacion = "${currentLocation!!.latitude};${currentLocation!!.longitude}"

                                if(idRestaurante > 0){
                                    restauranteService.actualizarRestaurante(restauranteAux!!).enqueue(
                                        object : Callback<Restaurante> {
                                            override fun onResponse(
                                                call: Call<Restaurante>,
                                                response: Response<Restaurante>
                                            ) {
                                                if(response!!.isSuccessful){
                                                    restauranteAux = response.body() as Restaurante
                                                    binding.txtUbicGeo.text = restauranteAux.geolocalizacion
                                                    Toast.makeText(activity!!, "Ubicacion actualizada", Toast.LENGTH_SHORT).show()
                                                }else{
                                                    Toast.makeText(activity!!, "No pudimos actualizar la ubicacion en la base de datos por intentalo mas tarde", Toast.LENGTH_SHORT).show()
                                                }
                                            }

                                            override fun onFailure(
                                                call: Call<Restaurante>,
                                                t: Throwable
                                            ) {
                                                Toast.makeText(activity!!, "No pudimos actualizar la ubicacion en la base de datos por intentalo mas tarde", Toast.LENGTH_SHORT).show()
                                            }
                                        }
                                    )
                                }else if(idBanco > 0){

                                }
                            }else{
                                Toast.makeText(requireActivity(), "No se pudo obtener la ubicacion actual por favor verifica que tienes la ubicacion activada o intentalo mas tarde", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }else{
            Toast.makeText(requireActivity(), "NO tiene opcion de geolocalizacion", Toast.LENGTH_SHORT).show()
        }
    }


    fun pedirPermisosUbicacion(view: View){
        if (view.context.packageManager.hasSystemFeature(
                PackageManager.FEATURE_LOCATION ) && view.context.packageManager.hasSystemFeature(
                PackageManager.FEATURE_LOCATION_NETWORK ) && view.context.packageManager.hasSystemFeature(
                PackageManager.FEATURE_LOCATION_GPS )) {

            if (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(
                    requireActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_DENIED
                    ) {
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION ), PERMISSION_CODE_UBICATION)
            }else{

            }
        }else{
            Toast.makeText(requireActivity(), "NO tiene opcion de geolocalizacion", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Toast.makeText(requireActivity(), "request code ${requestCode}", Toast.LENGTH_SHORT).show()
        when (requestCode) {
            PERMISSION_CODE_UBICATION -> {
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Toast.makeText(requireActivity(), "ubication Permission Granted", Toast.LENGTH_SHORT).show()
                    pedirPermisosUbicacion(binding.root)

                } else {
                    Toast.makeText(requireActivity(), "ubication Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }

            else -> {
                Toast.makeText(requireActivity(), "ubication Permission Denied 2", Toast.LENGTH_SHORT).show()
            }
        }


    }
}