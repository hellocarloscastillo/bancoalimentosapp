package com.example.bancoalimentosapp.Vehiculos

import java.io.Serializable

data class CarRow(
    var id_vehiculos:Int,
    var color: String,
    var placa: String,
    var modelo: String,
    var id_bancos_alimentos: Int
    ):Serializable
