package com.example.bancoalimentosapp.Vehiculos

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.services.VehiculoService
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListCarAdapter(private val mList: MutableList<CarRow>, val contex: Context, val fragment: Fragment): RecyclerView.Adapter<ListCarAdapter.ViewHolder>() {

    private val vehiculosService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<VehiculoService>(VehiculoService::class.java)

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListCarAdapter.ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_list_car, parent, false)

        return ListCarAdapter.ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ListCarAdapter.ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val ItemViewModel = mList[position]

        // sets the image to the imageview from our itemHolder class
        holder.modeloCar.text = ItemViewModel.modelo
        holder.colorCar.text = ItemViewModel.color
        holder.placaCar.text = ItemViewModel.placa

        holder.btnMenu.setOnClickListener { //creating a popup menu
            val popup = PopupMenu(contex, holder.btnMenu)
            //inflating menu from xml resource
            popup.inflate(R.menu.option_row_vehiculos)
            //adding click listener
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    return when (item.getItemId()) {
                        R.id.editCarRow ->   {
                            var carAux = Bundle()
                            carAux.putSerializable("carAux",ItemViewModel )
                            fragment.findNavController().navigate(R.id.nuevoVehiculo, carAux)
                            true
                        }

                        R.id.delCarRow -> {
                            vehiculosService.eliminarVehiculo(ItemViewModel).enqueue(
                                object : Callback<Any> {
                                    override fun onResponse(
                                        call: Call<Any>,
                                        response: Response<Any>
                                    ) {
                                        if(response.isSuccessful){
                                            Toast.makeText(contex, "Vehiculo eliminado", Toast.LENGTH_LONG).show()
                                        }else{
                                            Toast.makeText(contex, "No se pudo eliminar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                    override fun onFailure(call: Call<Any>, t: Throwable) {
                                        mList.remove(ItemViewModel)
                                        notifyItemRemoved(position)
                                        Toast.makeText(contex, "No se pudo eliminar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                                    }
                                }
                            )
                            true
                        }


                        else -> false
                    }
                }
            })
            //displaying the popup
            popup.show()
        }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val modeloCar: TextView = itemView.findViewById(R.id.modeloCar)
        val colorCar: TextView = itemView.findViewById(R.id.colorCar)
        val placaCar: TextView = itemView.findViewById(R.id.placaCar)
        val btnMenu: ImageButton = itemView.findViewById(R.id.btnOptionRowCar)
    }
}