package com.example.bancoalimentosapp.Vehiculos

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.VehiculosFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson

class Vehiculos : Fragment() {

    companion object {
        fun newInstance() = Vehiculos()
    }

    private lateinit var viewModel: VehiculosViewModel

    private var _binding: VehiculosFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = VehiculosFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = VehiculosViewModel(activity, this)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val recyclerListCar = binding.listCars
        recyclerListCar?.layoutManager = LinearLayoutManager(activity)




        viewModel.listarVehiculos(recyclerListCar)

        val btnAddCar: ImageButton = binding.btnAddCar

        btnAddCar.setOnClickListener {
            findNavController().navigate(R.id.nuevoVehiculo )
        }



    }

}