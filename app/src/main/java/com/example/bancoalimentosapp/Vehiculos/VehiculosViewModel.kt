package com.example.bancoalimentosapp.Vehiculos

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Vehiculo
import com.example.bancoalimentosapp.services.VehiculoService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class VehiculosViewModel(val activity: Activity?, val fragment:Fragment) {

    val vehiculoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<VehiculoService>(VehiculoService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    fun listarVehiculos(listaCar: RecyclerView){
        vehiculoService.vehiculosPorUsuario().enqueue(
            object: Callback<List<Vehiculo>>{
                override fun onResponse(
                    call: Call<List<Vehiculo>>,
                    response: Response<List<Vehiculo>>
                ) {
                    if(response!!.isSuccessful){
                        val listcar: MutableList<CarRow> = mutableListOf()
                        response!!.body()!!.forEach {
                            listcar.add(CarRow(it.id_vehiculos,it.color, it.placa, it.modelo, it.id_bancos_alimentos))
                        }
                        val adapter = ListCarAdapter(listcar, listaCar.context, fragment)
                        listaCar!!.adapter = adapter
                    }else{
                        Toast.makeText(listaCar.context, "Lo sentimos, no pudimos cargar los vehiculos, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<Vehiculo>>, t: Throwable) {
                    Toast.makeText(listaCar.context, "Lo sentimos, no pudimos cargar los vehiculos, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                }
            }
        )
    }
}