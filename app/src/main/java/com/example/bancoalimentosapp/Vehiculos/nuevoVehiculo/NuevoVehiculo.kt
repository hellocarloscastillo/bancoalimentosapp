package com.example.bancoalimentosapp.Vehiculos.nuevoVehiculo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import com.example.bancoalimentosapp.Vehiculos.CarRow
import com.example.bancoalimentosapp.databinding.NuevoVehiculoFragmentBinding

class NuevoVehiculo : Fragment() {

    companion object {
        fun newInstance() = NuevoVehiculo()
    }

    private lateinit var viewModel: NuevoVehiculoViewModel

    private var _binding: NuevoVehiculoFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = NuevoVehiculoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view)

    }

    fun initView(view: View){

        var carAux = arguments?.getSerializable("carAux")

        val color: EditText = binding.colorCar
        val placa: EditText = binding.placaCar
        val modelo: EditText = binding.modelCar
        val banco: Spinner = binding.spinnBankByUsr
        val btnCrear: Button = binding.guardarcar
        viewModel = NuevoVehiculoViewModel(activity)

        viewModel.cargarBancos(view, banco)

        if(carAux == null){
            initNewCar(btnCrear, color, placa, modelo, banco)
        }else{
            initUpdateCar(btnCrear, color, placa, modelo, banco, carAux as CarRow)
        }


    }

    fun initNewCar(btnCrear: Button, color: EditText, placa: EditText, modelo: EditText, banco: Spinner){

        btnCrear.setOnClickListener {
            viewModel.crearVehiculo(color, placa, modelo, banco)
        }

    }

    fun initUpdateCar(btnCrear: Button, color: TextView, placa: TextView, modelo: TextView, banco: Spinner, carAux:CarRow){
        binding.titleNuevoEmpleado.text = "Actualizar Vehiculo"
        btnCrear.text = "Actualizar"
        color.text = carAux.color
        placa.text = carAux.placa
        modelo.text = carAux.modelo
        /*viewModel.listBank.mapIndexed { index, bancoAux ->
            if(bancoAux.id_bancos_alimentos == carAux.id_bancos_alimentos){
                banco.setSelection(index)
            }
        }*/
        btnCrear.setOnClickListener {
            viewModel.actualizarVehiculo(carAux.id_vehiculos, color, placa, modelo, banco)
        }
    }


}