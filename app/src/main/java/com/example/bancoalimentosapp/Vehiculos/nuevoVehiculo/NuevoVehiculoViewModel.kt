package com.example.bancoalimentosapp.Vehiculos.nuevoVehiculo

import android.R
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.Vehiculo
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.services.VehiculoService
import com.example.bancoalimentosapp.utils.Settings
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NuevoVehiculoViewModel(val activity: Activity?) {

    val vehiculoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<VehiculoService>(VehiculoService::class.java)

    val empleadosService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<EmpleadoService>(EmpleadoService::class.java)

    val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    lateinit var listBank: List<Banco>

    fun cargarBancos(view: View, bancos: Spinner){

        bancoService.listarBnacosPorUsuario(usuarioSession.id_usuario).enqueue(
            object : Callback<List<Banco>> {
                override fun onResponse(
                    call: Call<List<Banco>>,
                    response: Response<List<Banco>>
                ) {
                    listBank = response!!.body()!!
                    val listTitleBank: MutableList<String> = mutableListOf()
                    listBank.forEach {
                        listTitleBank.add(it.nombre)
                    }

                    val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                        view.context,
                        R.layout.simple_spinner_item, listTitleBank
                    )

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    bancos.setAdapter(adapter)
                }

                override fun onFailure(call: Call<List<Banco>>?, t: Throwable?) {
                    TODO("Not yet implemented")
                }

            }
        )

    }

    fun crearVehiculo(color:TextView, placa:TextView, modelo:TextView, banco:Spinner){

        val bancoCar: Banco = listBank.get(banco.selectedItemPosition)
        val newCar: Vehiculo = Vehiculo(-1,color.text.toString(), placa.text.toString(),modelo.text.toString(),bancoCar.id_bancos_alimentos)
        val validateRespon = validateData(newCar)

        if(validateRespon.success){

            vehiculoService.guardarVehiculo(newCar).enqueue(
                object : Callback<Vehiculo>{
                    override fun onResponse(call: Call<Vehiculo>, response: Response<Vehiculo>) {
                        if(response.isSuccessful){
                            Snackbar.make(color, "Vehiculo guardado : " + response.body().toString(), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                        }else{
                            Snackbar.make(color, "No se pudo guardar el vehiculo, intentalo mas tarde", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                        }
                    }

                    override fun onFailure(call: Call<Vehiculo>, t: Throwable) {
                        Snackbar.make(color, "No se pudo guardar el vehiculo, intentalo mas tarde", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                }
            )
        }else{
            Snackbar.make(color, validateRespon.dato as String, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun actualizarVehiculo(id:Int,color:TextView, placa:TextView, modelo:TextView, banco:Spinner){

        val bancoCar: Banco = listBank.get(banco.selectedItemPosition)
        val newCar: Vehiculo = Vehiculo(id,color.text.toString(), placa.text.toString(),modelo.text.toString(),bancoCar.id_bancos_alimentos)
        val validateRespon = validateData(newCar)

        if(validateRespon.success){

            vehiculoService.actualizarVehiculo(newCar).enqueue(
                object : Callback<Vehiculo>{
                    override fun onResponse(call: Call<Vehiculo>, response: Response<Vehiculo>) {
                        if(response.isSuccessful){
                            Snackbar.make(color, "Datos Actualizados : " + response.body().toString(), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                        }else {
                            Snackbar.make(
                                color,
                                "No se pudo actualizar el vehiculo, intentalo mas tarde",
                                Snackbar.LENGTH_LONG
                            )
                                .setAction("Action", null).show()
                        }
                    }
                    override fun onFailure(call: Call<Vehiculo>, t: Throwable) {
                        Snackbar.make(color, "No se pudo actualizar el vehiculo, intentalo mas tarde", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                }
            )
        }else{
            Snackbar.make(color, validateRespon.dato as String, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun validateData(newCar: Vehiculo):validateResponse{
        if(newCar.color.equals("") || newCar.color.equals(null)){
           return  validateResponse(false, "Digite un color")
        }
        if(newCar.placa.equals("") || newCar.placa.equals(null)){
            return  validateResponse(false, "Digite el codigo de la matricula del vehiculo")
        }
        if(newCar.modelo.equals("") || newCar.modelo.equals(null)){
            return  validateResponse(false, "Digite un modelo")
        }
        if(newCar.id_bancos_alimentos == null || newCar.id_bancos_alimentos < 1){
            return  validateResponse(false, "Seleccione un Banco")
        }
        return validateResponse(true, newCar)
    }

    data class validateResponse(val success: Boolean, val dato: Any)
}