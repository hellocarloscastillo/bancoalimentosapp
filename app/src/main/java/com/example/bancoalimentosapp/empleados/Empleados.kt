package com.example.bancoalimentosapp.empleados

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.EmpleadosFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Empleado

import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Empleados : Fragment() {

    companion object {
        fun newInstance() = Empleados()
    }

    private lateinit var viewModel: EmpleadosViewModel
    private var _binding: EmpleadosFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = EmpleadosFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val btnAddEMployee: ImageButton = binding.imageButton
        viewModel = EmpleadosViewModel(activity)

        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)
        var appbar = activity?.findViewById(R.id.appbarlayout) as AppBarLayout
        appbar.visibility = View.VISIBLE
        appbar.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;

        var nameUsr: TextView = activity?.findViewById(R.id.nameUsrMenu) as TextView
        nameUsr.text = usuario.nombre

        var emailUsr: TextView = activity?.findViewById(R.id.emailUsrMenu) as TextView
        emailUsr.text = usuario.correo

        //init list recycler view
        val recyclerListEmployee = activity?.findViewById<RecyclerView>(R.id.listEmployee)
        recyclerListEmployee?.layoutManager = LinearLayoutManager(activity)

        viewModel.empleadoService.empleadosPorUsuario(usuario.id_usuario).enqueue(
            object : Callback<List<Empleado>> {

                override fun onResponse(call: Call<List<Empleado>>?, response: Response<List<Empleado>>?) {
                    if(response!!.isSuccessful){
                        val listaEmpleados: MutableList<EmployeRow> = mutableListOf()
                        response!!.body()!!.forEach {
                            listaEmpleados.add(EmployeRow(it.id_empleados, it.nombre, it.documento.toString(),it.edad,it.id_vehiculos,it.banco_alimento))
                        }
                        val adapter = ListEmployeAdapter(listaEmpleados, context!!, this@Empleados)
                        recyclerListEmployee!!.adapter = adapter
                    }else{
                        Toast.makeText(activity, "NO se pudieron cargar los empleados, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<Empleado>>?, t: Throwable?) {
                    Toast.makeText(context, "NO se pudieron cargar los empleados", Toast.LENGTH_LONG).show()
                }
            }
        )


        btnAddEMployee.setOnClickListener {
            println("hola ? ")
            findNavController().navigate(R.id.nuevoEmpleado )
        }

    }

}