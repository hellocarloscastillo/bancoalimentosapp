package com.example.bancoalimentosapp.empleados

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class EmpleadosViewModel(val activity: Activity?)  {

    public val empleadoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<EmpleadoService>(EmpleadoService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)



}