package com.example.bancoalimentosapp.empleados

import java.io.Serializable

data class EmployeRow(
    var id_empleados:Int,
    var nombre: String,
    var documento: String,
    var edad: Int,
    var id_vehiculos: Int,
    var banco_alimento: Int
    ):Serializable
