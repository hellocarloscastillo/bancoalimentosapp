package com.example.bancoalimentosapp.empleados

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.Bancos.BancoRow
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ListEmployeAdapter (private val mList: MutableList<EmployeRow>, val contex:Context, private val fragment: Fragment): RecyclerView.Adapter<ListEmployeAdapter.ViewHolder>() {

    private val empleadoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<EmpleadoService>(EmpleadoService::class.java)
    lateinit var dialog: Dialog;
    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_list_employee, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val ItemViewModel = mList[position]
        dialog = Dialog(contex)

        // sets the image to the imageview from our itemHolder class
        holder.idEmployee.text = ItemViewModel.id_empleados.toString()
        holder.nameEmployee.text = ItemViewModel.nombre

        holder.emailEmployee.text = ItemViewModel.documento

        holder.btnMenu.setOnClickListener { //creating a popup menu
            val popup = PopupMenu(contex, holder.btnMenu)
            //inflating menu from xml resource
            popup.inflate(R.menu.option_row_employee)
            //adding click listener
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    return when (item.getItemId()) {
                        R.id.editEmpList ->       {
                            var empleadoUpdate = Bundle()
                            empleadoUpdate.putSerializable("empleadoUpdate", ItemViewModel )
                            fragment.findNavController().navigate(R.id.nuevoEmpleado, empleadoUpdate)
                            true
                        }

                        R.id.delEmpList -> {
                            openOptionRegister(ItemViewModel, position)
                            true

                        }
                        else -> false
                    }
                }
            })
            //displaying the popup
            popup.show()
        }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val idEmployee: TextView = itemView.findViewById(R.id.idEmployee)
        val nameEmployee: TextView = itemView.findViewById(R.id.nameEmployee)
        val emailEmployee: TextView = itemView.findViewById(R.id.emailEmployee)
        val btnMenu: ImageButton = itemView.findViewById(R.id.btnOption)
    }

    private fun openOptionRegister(ItemViewModel: EmployeRow, position: Int){
        dialog.setContentView(R.layout.mensaje_confirmacion)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        var btnYes = dialog.findViewById<Button>(R.id.btnYesDialogConfirm)
        var btnNo = dialog.findViewById<Button>(R.id.btnNoDialogConfirm)
        var text = dialog.findViewById<TextView>(R.id.textDialogCOnfirm)
        text.text = "Seguro que sea eliminar el empleado ${ItemViewModel.nombre}"
        btnYes.setOnClickListener {
            empleadoService.eliminarEmpleado(ItemViewModel).enqueue(
                object : Callback<Any> {
                    override fun onResponse(
                        call: Call<Any>,
                        response: Response<Any>
                    ) {
                        if(response.isSuccessful){
                            Toast.makeText(contex, "Empleado eliminado", Toast.LENGTH_LONG).show()
                        }else{
                            Toast.makeText(contex, "No se pudo eliminar el Empleado, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<Any>, t: Throwable) {
                        mList.remove(ItemViewModel)
                        notifyItemRemoved(position)
                        Toast.makeText(contex, "No se pudo eliminar el Empleado, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )

        }

        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}