package com.example.bancoalimentosapp.empleados.nuevoEmpleado

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.bancoalimentosapp.databinding.NuevoEmpleadoFragmentBinding
import androidx.core.text.set
import com.example.bancoalimentosapp.empleados.EmployeRow
import com.example.bancoalimentosapp.models.Empleado


class NuevoEmpleado : Fragment() {

    companion object {
        fun newInstance() = NuevoEmpleado()
    }

    private lateinit var viewModel: NuevoEmpleadoViewModel
    private var _binding: NuevoEmpleadoFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = NuevoEmpleadoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initView(view)

    }

    fun initView(view: View){

        var empleadoAux = arguments?.getSerializable("empleadoUpdate")
        val nombre: EditText = binding.nameEmployee
        val documento: EditText = binding.docEmployee
        val edad: EditText = binding.ageEmployee
        val banco: Spinner = binding.spinnBanco

        val colorVehiculoUsuario = binding.colorVehiculoUsuario
        val placaVehiculoUsuario = binding.placaVehiculoUsuario
        val modeloVehiculoUsuario = binding.modeloVehiculoUsuario


        val btnCrear: Button = binding.saveEmployee
        viewModel = NuevoEmpleadoViewModel(activity, this)
        viewModel.cargarBancos(view, banco)


       /* banco.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                vieww: View,
                position: Int,
                id: Long
            ) {
                viewModel.cargarVehiculos(vieww, vehiculo, viewModel.listBank[position].id_bancos_alimentos)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })*/


        if(empleadoAux == null){
            initNuevoEmpleado(banco, colorVehiculoUsuario,placaVehiculoUsuario, modeloVehiculoUsuario, btnCrear,nombre,documento,edad)
        }
        else{
            initEditarEmpleado(banco,colorVehiculoUsuario ,placaVehiculoUsuario,modeloVehiculoUsuario, btnCrear,nombre,documento,edad, empleadoAux as EmployeRow)
        }


    }

    fun initNuevoEmpleado(banco: Spinner, color:EditText, placa:EditText, modelo:EditText, btnCrear:Button, nombre:EditText, documento:EditText,edad:EditText){

        btnCrear.setOnClickListener {
            viewModel.guardarEmpleado(
                nombre,
                documento,
                edad,
                color,
                placa,
                modelo,
                banco
            )
        }

    }
    fun initEditarEmpleado(banco: Spinner, color:TextView, placa:TextView, modelo:TextView, btnCrear:Button, nombre:TextView, documento:TextView,edad:TextView,empleadoAux:EmployeRow){

        nombre.setText(empleadoAux.nombre)
        documento.text = empleadoAux.documento
        edad.text = empleadoAux.edad.toString()
        btnCrear.text = "Actualizar"
        binding.titleNuevoEmpleado.text = "Actualizar Empleado"

        viewModel.cargarVehiculo(empleadoAux.id_vehiculos, empleadoAux.banco_alimento, color ,placa, modelo)

        btnCrear.setOnClickListener {
            viewModel.actualizarEmpleado(
                empleadoAux.id_empleados,
                empleadoAux.id_vehiculos,
                nombre,
                documento,
                edad,
                color,
                placa,
                modelo,
                banco
            )
        }
    }

}