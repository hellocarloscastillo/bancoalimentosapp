package com.example.bancoalimentosapp.empleados.nuevoEmpleado

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.Empleado
import com.example.bancoalimentosapp.models.Vehiculo
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.services.VehiculoService
import com.example.bancoalimentosapp.utils.Settings
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


data class validateResponse(val success: Boolean, val dato:Any)

class NuevoEmpleadoViewModel (val activity: Activity?, val fragment: Fragment){

    val empleadoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<EmpleadoService>(EmpleadoService::class.java)

    val vehiculoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<VehiculoService>(VehiculoService::class.java)

    val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    lateinit var listCar: List<Vehiculo>

    lateinit var listBank: List<Banco>

    fun cargarVehiculos(view: View, vehiculo: Spinner, idBank: Int){

        vehiculoService.vehiculosPorBanco(idBank).enqueue(
            object : Callback<List<Vehiculo>> {
                override fun onResponse(
                    call: Call<List<Vehiculo>>,
                    response: Response<List<Vehiculo>>
                ) {
                    if(response.isSuccessful){
                        listCar = response!!.body()!!
                        val listTitleCar: MutableList<String> = mutableListOf()
                        listCar.forEach {
                            listTitleCar.add("${it.modelo} - ${it.placa}")
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            view.context,
                            android.R.layout.simple_spinner_item, listTitleCar
                        )

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        vehiculo.setAdapter(adapter)
                    }else{
                        Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de autos", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<Vehiculo>>?, t: Throwable?) {
                    Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de autos", Toast.LENGTH_LONG).show()
                }

            }
        )

    }

    fun cargarBancos(view: View, bancos: Spinner){

        bancoService.listarBnacosPorUsuario(usuarioSession.id_usuario).enqueue(
            object : Callback<List<Banco>> {
                override fun onResponse(
                    call: Call<List<Banco>>,
                    response: Response<List<Banco>>
                ) {
                    if(response.isSuccessful){
                        listBank = response!!.body()!!
                        val listTitleBank: MutableList<String> = mutableListOf()
                        listBank.forEach {
                            listTitleBank.add(it.nombre)
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            view.context,
                            android.R.layout.simple_spinner_item, listTitleBank
                        )

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        bancos.setAdapter(adapter)
                    }else{
                        Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de bancos", Toast.LENGTH_LONG).show()
                    }
                }
                override fun onFailure(call: Call<List<Banco>>?, t: Throwable?) {
                    Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de bancos", Toast.LENGTH_LONG).show()
                }

            }
        )

    }

    fun guardarEmpleado(nombre:EditText, documento:EditText, edad:EditText, color:EditText, placa:EditText, modelo:EditText, bancos: Spinner ){
        val newEmploye: Empleado = Empleado(
            -1,nombre.text.toString(),
            if (documento.text.toString().equals("")) -1 else documento.text.toString().toInt(),
            if (edad.text.toString().equals("")) -1 else edad.text.toString().toInt(),
            -1,
            listBank.get(bancos.selectedItemPosition).id_bancos_alimentos
        )

        val newVehiculo: Vehiculo = Vehiculo(
            -1,
            color.text.toString(),
            placa.text.toString(),
            modelo.text.toString(),
            listBank.get(bancos.selectedItemPosition).id_bancos_alimentos
        )
        //Toast.makeText(nombre.context, "Vehiculo a guardar : \n${newVehiculo.toString()}", Toast.LENGTH_LONG).show()
        val responseValidate = validateEmploye(newEmploye, newVehiculo)

        if(responseValidate.success){
            vehiculoService.guardarVehiculo(newVehiculo).enqueue(
                object : Callback<Vehiculo> {
                    override fun onResponse(call: Call<Vehiculo>, response: Response<Vehiculo>) {
                        if (response.isSuccessful) {
                            Toast.makeText(nombre.context, "Vehiculo Guardado", Toast.LENGTH_LONG).show()
                            newEmploye.id_vehiculos = response.body()!!.id_vehiculos
                            empleadoService.guardarEmpleado(newEmploye).enqueue(
                                object: Callback<Empleado> {
                                    override fun onResponse(call: Call<Empleado>, response: Response<Empleado>) {
                                        if(response.isSuccessful){
                                            Snackbar.make(nombre, "Empleado Guardado ", Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show()
                                            fragment.findNavController().navigate(R.id.empleados)
                                        }else{
                                            Toast.makeText(nombre.context, "Error inesperado, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                                        }

                                    }

                                    override fun onFailure(call: Call<Empleado>, t: Throwable) {
                                        Toast.makeText(nombre.context, "Error inesperado, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                                    }
                                }
                            )
                        }else{
                            Toast.makeText(nombre.context, "No pudimos guardar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<Vehiculo>, t: Throwable) {
                        println("error vehiculo : \n " + t.stackTrace.toString())
                        Toast.makeText(nombre.context, "No pudimos guardar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )
        }else{
            Snackbar.make(nombre, responseValidate.dato as String, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }


    }

    fun actualizarEmpleado(idEmpleado :Int, idVehiculo:Int, nombre: TextView, documento:TextView, edad:TextView,  color:TextView, placa:TextView, modelo:TextView, bancos: Spinner ){
        val newEmploye: Empleado = Empleado(
            idEmpleado,nombre.text.toString(),
            if (documento.text.toString().equals("")) -1 else documento.text.toString().toInt(),
            if (edad.text.toString().equals("")) -1 else edad.text.toString().toInt(),
            idVehiculo,
            listBank.get(bancos.selectedItemPosition).id_bancos_alimentos
        )

        val vehiculoUpdate: Vehiculo = Vehiculo(
            idVehiculo,
            color.text.toString(),
            placa.text.toString(),
            modelo.text.toString(),
            listBank.get(bancos.selectedItemPosition).id_bancos_alimentos
        )

        val responseValidate = validateEmploye(newEmploye, vehiculoUpdate)

        if(responseValidate.success){
            empleadoService.actualizarEmpleado(newEmploye).enqueue(
                object: Callback<Empleado> {
                    override fun onResponse(call: Call<Empleado>, response: Response<Empleado>) {
                        if(response.isSuccessful){
                            Snackbar.make(nombre, "Empleado Actualizado ", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                            vehiculoService.actualizarVehiculo(vehiculoUpdate).enqueue(
                                object : Callback<Vehiculo> {
                                    override fun onResponse(
                                        call: Call<Vehiculo>,
                                        response: Response<Vehiculo>
                                    ) {
                                        if(response.isSuccessful){
                                            Snackbar.make(nombre, "Vehiculo Actualizado ", Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show()
                                            fragment.findNavController().navigate(R.id.empleados)
                                        }else{
                                            Toast.makeText(nombre.context, "Error al actualizar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                    override fun onFailure(call: Call<Vehiculo>, t: Throwable) {
                                        Toast.makeText(nombre.context, "Error al actualizar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                                    }
                                }
                            )
                        }else{
                            Toast.makeText(nombre.context, "Error inesperado, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<Empleado>, t: Throwable) {
                        Toast.makeText(nombre.context, "NO pudimos actualizar los datos, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )
        }else{
            Snackbar.make(nombre, responseValidate.dato as String, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }


    }

    fun cargarVehiculo(idVehiculo:Int, idBAnk:Int, color:TextView, placa:TextView, modelo:TextView){
        vehiculoService.vehiculosPorBanco(idBAnk).enqueue(
            object :Callback<List<Vehiculo>> {
                override fun onResponse(
                    call: Call<List<Vehiculo>>,
                    response: Response<List<Vehiculo>>
                ) {
                   if(response.isSuccessful){
                       var lisAuxVehiculos = response.body() as List<Vehiculo>
                       if(lisAuxVehiculos.size > 0){
                           lisAuxVehiculos.mapIndexed { index, vehiculoAux ->

                               if(vehiculoAux.id_vehiculos == idVehiculo){
                                   color.text = vehiculoAux.color
                                   placa.text = vehiculoAux.placa
                                   modelo.text = vehiculoAux.modelo
                               }
                           }
                       }else{
                           Toast.makeText(color.context, "El Banco NO tiene Vehiculos", Toast.LENGTH_LONG).show()
                       }
                   }else{
                       Toast.makeText(color.context, "Error al consultar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                   }
                }

                override fun onFailure(call: Call<List<Vehiculo>>, t: Throwable) {
                    Toast.makeText(color.context, "Error al consultar el vehiculo, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                }
            }
        )
    }


    fun validateEmploye(newEmploye: Empleado, vehiculo:Vehiculo): validateResponse{
        if(newEmploye.nombre.equals("") || newEmploye.nombre.length < 3){
            return validateResponse(false, "Nombre incorrecto")
        }
        if(newEmploye.documento == null || newEmploye.documento < 9999){
            return validateResponse(false, "Documento incorrecto, muy pocos dugitos")
        }
        if(newEmploye.edad < 18 || newEmploye.edad == null){
            return validateResponse(false, "Edad incorrecta, debe ser mayor de 18 años o ingrese un valor")
        }
        if(vehiculo.color.equals("")){
            return validateResponse(false, "Color del Vehiculo incorrecto")
        }
        if(vehiculo.placa.equals("")){
            return validateResponse(false, "Placa del Vehiculo incorrecto")
        }
        if(vehiculo.modelo.equals("")){
            return validateResponse(false, "Modelo del Vehiculo incorrecto")
        }
        if(newEmploye.banco_alimento == null){
            return validateResponse(false, "Banco incorrecto, no hay un banco seleccionado")
        }

        return validateResponse(true, newEmploye)
    }

}