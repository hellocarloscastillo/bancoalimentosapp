package com.example.bancoalimentosapp.login.views.data

import com.example.bancoalimentosapp.login.views.data.model.LoggedInUser
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.services.UsuarioService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            // TODO: handle loggedInUser authentication

            //val call = getService().create(UsuarioService::class.java).login("/usuarios/$username/$password").execute()
            //val usuarioSesionAux = call.body() as UsuarioSesion

            val fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), "carlos castillo")
            return Result.Success(fakeUser)
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }


}