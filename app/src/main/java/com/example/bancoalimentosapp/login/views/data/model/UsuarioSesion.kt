package com.example.bancoalimentosapp.login.views.data.model

import com.example.bancoalimentosapp.models.TipoUsuario
import java.io.Serializable

data class UsuarioSesion(
    var id_usuario: Int,
    var nombre: String,
    var contrasena: String,
    var correo: String,
    var documento: Int,
    var tipoUsuario: TipoUsuario,
    var id_tipo_usuario:Int
):Serializable
