package com.example.bancoalimentosapp.login.views.ui.login

import android.app.AlarmManager
import android.app.Dialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ImageButton
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import com.example.bancoalimentosapp.MainActivity
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.FragmentLoginBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson


class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel
    private var _binding: FragmentLoginBinding? = null
    lateinit var dialog: Dialog;

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var usuario: UsuarioSesion

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return

        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)

        val usernameEditText = binding.email
        val passwordEditText = binding.passwd
        val loginButton = binding.btnlogin
        val registerText = binding.createnewac
        val loadingProgressBar = binding.loading

        dialog = Dialog(view.context)

        loginViewModel.loginFormState.observe(viewLifecycleOwner,
            Observer { loginFormState ->
                if (loginFormState == null) {
                    return@Observer
                }
                loginButton.isEnabled = loginFormState.isDataValid
                loginFormState.usernameError?.let {
                    usernameEditText.error = getString(it)
                }
                loginFormState.passwordError?.let {
                    passwordEditText.error = getString(it)
                }
            })

        loginViewModel.loginResult.observe(viewLifecycleOwner,
            Observer { loginResult ->
                loginResult ?: return@Observer
                loadingProgressBar.visibility = View.GONE
                loginResult.error?.let {
                    showLoginFailed(it)
                }
                loginResult.success?.let {
                    updateUiWithUser(it)
                }
            })

        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable) {
                loginViewModel.loginDataChanged(
                    usernameEditText.text.toString(),
                    passwordEditText.text.toString()
                )
            }
        }
        usernameEditText.addTextChangedListener(afterTextChangedListener)
        passwordEditText.addTextChangedListener(afterTextChangedListener)
        passwordEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                loginViewModel.login(
                    usernameEditText.text.toString(),
                    passwordEditText.text.toString(),
                    activity
                )
            }
            false
        }

        loginButton.setOnClickListener {
            loadingProgressBar.visibility = View.VISIBLE
            try{
                loginViewModel.login(
                    usernameEditText.text.toString(),
                    passwordEditText.text.toString(),
                    activity
                )
            }
            catch (e:Exception){
                val appContext = context?.applicationContext
                Toast.makeText(appContext, "Por favor intentalo mas tarde", Toast.LENGTH_LONG).show()
            }


        }

        registerText.setOnClickListener{
            this.openOptionRegister()
        }
        //loginViewModel.checkLogin(usuario, this)
    }


    private fun openOptionRegister(){
        dialog.setContentView(R.layout.banco_restaurant_opcion)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val btnok = dialog.findViewById<ImageButton>(R.id.btnBancoOption)
        val btncancel = dialog.findViewById<ImageButton>(R.id.btnRestauranteOption)
        //eventos dialog
        var typeUsr = Bundle()
        btnok.setOnClickListener{
            typeUsr.putInt("typeUsr",2)
            findNavController().navigate(R.id.registroUsuarios, typeUsr )
            dialog.dismiss()
        }
        btncancel.setOnClickListener{
            typeUsr.putInt("typeUsr",1)
            findNavController().navigate(R.id.registroUsuarios, typeUsr )
            dialog.dismiss()

        }

        dialog.show()
    }

    fun changeMenuByUsr(): NavigationView {

        val nv: NavigationView = requireActivity().findViewById(R.id.nav_view)
        nv.menu.clear()
        nv.inflateMenu(R.menu.menu_restaurante)
        return nv

    }

    fun changeOptionMenuByUsr(): Set<Int> {
            return setOf(
                R.id.listaRestaurante, R.id.pedidosRest, R.id.sobrasRestaurante
            )


    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome) + model.displayName
        // TODO : initiate successful logged in experience

        var is_login = Bundle()

        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)
        if(usuario.tipoUsuario.id_tipo_usuario != 0 && usuario.tipoUsuario.id_tipo_usuario ==1){
            var navView = changeMenuByUsr()
            val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
            var appBarConfiguration: AppBarConfiguration = AppBarConfiguration(
                changeOptionMenuByUsr() , drawerLayout)
            

            navView.setNavigationItemSelectedListener {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START)
                } else {
                    drawerLayout.openDrawer(GravityCompat.START)
                }
                when(it.itemId) {

                    R.id.restaurantes -> {
                        findNavController().navigate(R.id.listaRestaurante)
                        true
                    }

                    R.id.sobras -> {
                        findNavController().navigate(R.id.sobrasRestaurante)
                        true
                    }

                    R.id.pedidosRest -> {
                        findNavController().navigate(R.id.pedidosRestaurante)
                        true
                    }


                    R.id.loginFragment -> {
                        var appbar = requireActivity().findViewById(R.id.appbarlayout) as AppBarLayout
                        appbar.visibility = View.INVISIBLE
                        appbar.getLayoutParams().height= 0;
                        var usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)
                        println("usuario de la monda : ${usuario.correo}")
                        sharedPreferences.edit().remove("usuario").commit()
                        requireActivity().finishAffinity();
                        true
                    }

                    else ->{
                        true
                    }

                }
            }


            findNavController().popBackStack(R.id.listaRestaurante, true)
            is_login.putBoolean("is_login", true)
            findNavController().navigate(R.id.listaRestaurante, is_login )

        }
        if(usuario.tipoUsuario.id_tipo_usuario != 0 && usuario.tipoUsuario.id_tipo_usuario ==2){

            findNavController().popBackStack(R.id.action_loginFragment_to_empleados, true)
            is_login.putBoolean("is_login", true)
            findNavController().navigate(R.id.action_loginFragment_to_empleados, is_login )
        }
        val layout: Dialog = Dialog(requireContext())
        layout.setContentView(R.layout.bienvenida_login)
        layout.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, welcome, Toast.LENGTH_LONG).show()
        Thread.sleep(3000L)
        layout.dismiss()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    //override fun onCreate(savedInstanceState: Bundle?) {
    //    super.onCreate(savedInstanceState)
    //    var usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", null), UsuarioSesion::class.java)
    //    if( usuarioSession.id_usuario != null ){
    //        println("usuario id : ${usuarioSession.id_usuario}")
    //        val navOptions = NavOptions.Builder()
    //            .setPopUpTo(R.id.empleados, true)
    //            .build()
    //        findNavController().navigate(R.id.empleados  )
//
    //    }
    //}
}