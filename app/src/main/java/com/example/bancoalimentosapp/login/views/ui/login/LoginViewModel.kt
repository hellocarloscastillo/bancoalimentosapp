package com.example.bancoalimentosapp.login.views.ui.login

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.login.views.data.LoginRepository
import com.example.bancoalimentosapp.login.views.data.Result
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.TipoUsuario
import com.example.bancoalimentosapp.services.UsuarioService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    private val loginService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
        .create<UsuarioService>(UsuarioService::class.java)

    fun login(username: String, password: String, activity: Activity?){

        this.loginService.login(username, password).enqueue(
            object : Callback<UsuarioSesion>{
                override fun onResponse(
                    call: Call<UsuarioSesion>,
                    response: Response<UsuarioSesion>
                ) {
                    try {
                        if(response!!.isSuccessful()){

                            var usrSesison = response?.body() as UsuarioSesion
                            usrSesison.tipoUsuario = TipoUsuario(usrSesison.id_tipo_usuario as Int, "")
                            val nombre =usrSesison.nombre
                            val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)
                            val editor: SharedPreferences.Editor =  sharedPreferences.edit()
                            editor.putString("usuario", Gson().toJson( usrSesison))
                            editor.apply()
                            editor.commit()
                            _loginResult.value =
                                LoginResult(success = LoggedInUserView(displayName = nombre))
                        }else{
                            Toast.makeText(activity!!, "Usuario o contraseña incorrectos", Toast.LENGTH_LONG).show()
                        }

                    }
                    catch (e:Exception ){
                        println("error login: "+ e.toString())
                        Toast.makeText(activity!!, "Ocurrio Un problema, por favor intentalo mas tarde\ncodigo de error : ${e?.toString()}", Toast.LENGTH_LONG).show()
                    }

                }
                override fun onFailure(call: Call<UsuarioSesion>, t: Throwable?) {
                    _loginResult.value = LoginResult(error = R.string.login_failed)
                }


            })
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains("@")) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }


    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 3
    }
}