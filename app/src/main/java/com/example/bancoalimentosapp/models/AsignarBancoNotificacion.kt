package com.example.bancoalimentosapp.models

data class AsignarBancoNotificacion(
    var id_bancos_alimentos:Int,
    var id_notificaciones:Int
)
