package com.example.bancoalimentosapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Banco(
    var id_bancos_alimentos:Int,
    var nombre:String,
    var id_usuario:Int,
    var estado: Boolean
    ): Serializable
