package com.example.bancoalimentosapp.models

data class Empleado(
    val id_empleados: Int,
    val nombre: String,
    val documento: Int,
    val edad: Int,
    var id_vehiculos: Int,
    val banco_alimento: Int
)
