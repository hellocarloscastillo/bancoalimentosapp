package com.example.bancoalimentosapp.models

import java.io.Serializable

data class IdBancoAlimentos(
    var id_bancos_alimentos:Int,
):Serializable
