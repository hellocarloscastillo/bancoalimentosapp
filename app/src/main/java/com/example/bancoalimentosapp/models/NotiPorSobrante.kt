package com.example.bancoalimentosapp.models

import java.io.Serializable

data class NotiPorSobrante(
    var id_notificaciones: Int,
    var id_restaurantes: Int
): Serializable
