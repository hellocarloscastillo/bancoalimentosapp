package com.example.bancoalimentosapp.models

data class Notificacion(
    var id_notificaciones: Int,
    var estado: String,
    var id_bancos_alimentos: Int,
    var id_sobrantes: Int,
    var id_restaurantes: Int,
    var horaCierreRestaurante: String
)
