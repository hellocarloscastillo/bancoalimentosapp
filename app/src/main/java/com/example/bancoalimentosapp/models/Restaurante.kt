package com.example.bancoalimentosapp.models

import java.sql.Time

data class Restaurante(
    val id_restaurantes: Int,
    val nombre : String,
    val dirección: String,
    var geolocalizacion: String,
    val telefono: Int,
    val horarios:String,
    val id_usuario: Int,
    val id_tipo_restaurante: Int
)
