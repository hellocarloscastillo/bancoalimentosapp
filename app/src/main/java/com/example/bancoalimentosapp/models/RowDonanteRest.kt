package com.example.bancoalimentosapp.models

import java.io.Serializable

data class RowDonanteRest(
    var id_sobrantes: Int,
    var cantidad: Int,
    var nombre: String,
    var id_tipo_sobrante: Int,
    var id_restaurantes: Int,
    var nombreRestaurante: String,
    var nombreSobrante: String
): Serializable
