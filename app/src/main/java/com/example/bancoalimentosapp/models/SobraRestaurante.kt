package com.example.bancoalimentosapp.models

data class SobraRestaurante(
    var id_sobra_restaurantes:Int,
    var id_restaurantes:Int,
    var id_sobrantes:Int
)
