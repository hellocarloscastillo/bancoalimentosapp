package com.example.bancoalimentosapp.models

data class SobranteGet(
    var id_sobrantes: Int,
    var cantidad: Double,
    var nombre: String,
    var id_tipo_sobrante: Int,
    var id_restaurantes: Int,
    var nombreRestaurante: String,
    var nombreSobrante: String
)
