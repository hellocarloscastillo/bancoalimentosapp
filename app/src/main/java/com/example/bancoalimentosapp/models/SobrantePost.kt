package com.example.bancoalimentosapp.models

data class SobrantePost(
    val cantidad: Double,
    var nombre: String,
    var id_tipo_sobrante: Int,
    var id_restaurantes:Int
)
