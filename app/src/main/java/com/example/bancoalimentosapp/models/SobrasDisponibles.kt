package com.example.bancoalimentosapp.models

data class SobrasDisponibles(
    val id_sobrantes: Int,
    val cantidad: Int,
    val nombre:String,
    val id_tipo_sobrante: Int,
)
