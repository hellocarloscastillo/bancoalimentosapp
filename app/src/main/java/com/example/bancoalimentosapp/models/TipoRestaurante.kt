package com.example.bancoalimentosapp.models

data class TipoRestaurante(
    val id_tipo_restaurante: Int,
    val nombre: String
)
