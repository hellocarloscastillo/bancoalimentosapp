package com.example.bancoalimentosapp.models

data class TipoSobrante(
    var id_tipo_sobrante: Int,
    var nombre: String
)
