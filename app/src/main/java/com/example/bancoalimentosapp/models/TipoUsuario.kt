package com.example.bancoalimentosapp.models

import java.io.Serializable

data class TipoUsuario(
    var id_tipo_usuario: Int,
    var nombre: String
):Serializable
