package com.example.bancoalimentosapp.models

data class Vehiculo(
    val id_vehiculos: Int,
    val color: String,
    val placa: String,
    val modelo: String,
    val id_bancos_alimentos: Int

)
