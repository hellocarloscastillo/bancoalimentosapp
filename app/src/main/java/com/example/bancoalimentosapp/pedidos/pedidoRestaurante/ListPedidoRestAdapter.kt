package com.example.bancoalimentosapp.pedidos.pedidoRestaurante

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.models.Notificacion
import com.example.bancoalimentosapp.services.NotificacionesSercvice
import com.example.bancoalimentosapp.utils.EventRecyclerView
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListPedidoRestAdapter (val mList: MutableList<RowPedidoRest>, val fragment: Fragment, val event: EventRecyclerView): RecyclerView.Adapter<ListPedidoRestAdapter.ViewHolder>() {

    val notificacionesService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<NotificacionesSercvice>(NotificacionesSercvice::class.java)
    lateinit var dialog: Dialog;
    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pedido_rest, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val ItemViewModel = mList[position]
        holder.txtIdPedidoRes.text = ItemViewModel.id.toString()
        holder.txtSobranteRest.text = ItemViewModel.nombreSobrante
        holder.txtEstadoPedidosRest.text = ItemViewModel.estado
        holder.txtBancoRest.text = ItemViewModel.banco
        dialog = Dialog(fragment.requireContext())
        dialog.setContentView(R.layout.mensaje_confirmacion)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var btnYes = dialog.findViewById<Button>(R.id.btnYesDialogConfirm)
        var btnNo = dialog.findViewById<Button>(R.id.btnNoDialogConfirm)
        var text = dialog.findViewById<TextView>(R.id.textDialogCOnfirm)

        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        if(ItemViewModel.estado.equals("Ejecucion") || ItemViewModel.estado.equals("Pendiente")){
            holder.btnOption.setOnClickListener {
                val popup = PopupMenu(fragment.context, holder.btnOption)
                popup.inflate(R.menu.option_row_pedidos_restaurante)
                var notiAux:Notificacion = Notificacion(ItemViewModel.id,ItemViewModel.estado,ItemViewModel.id_banco, ItemViewModel.nombreSobrante.toInt(), -1, "")
                popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem): Boolean {
                        return when (item.getItemId()) {
                            R.id.finalPedidoRest ->       {


                                text.text = "Seguro desea Finalizar el pedido ? "
                                dialog.show()
                                btnYes.setOnClickListener {
                                    notificacionesService.cambioEstadoTerminado(notiAux).enqueue(
                                        object : Callback<String> {
                                            override fun onResponse(
                                                call: Call<String>,
                                                response: Response<String>
                                            ) {
                                                if(response.isSuccessful){
                                                    dialog.dismiss()
                                                    Toast.makeText(fragment.context, response as String, Toast.LENGTH_LONG).show()

                                                }else{
                                                    dialog.dismiss()
                                                    Toast.makeText(fragment.context, "No pudimos dar por terminado el pedido, intentalo en un momento ", Toast.LENGTH_LONG).show()
                                                }
                                            }
                                            override fun onFailure(call: Call<String>, t: Throwable) {
                                                mList.remove(ItemViewModel)
                                                notifyItemRemoved(position)
                                                dialog.dismiss()
                                                Toast.makeText(fragment.context, "No pudimos dar por terminado el pedido, intentalo en un momento ", Toast.LENGTH_LONG).show()
                                            }
                                        }
                                    )
                                }
                                true
                            }

                            R.id.cancelPedidoRest -> {
                                text.text = "Seguro que sea Cancelar el pedido"
                                dialog.show()
                                btnYes.setOnClickListener {
                                    notificacionesService.cambioEstadoCancelado(notiAux).enqueue(
                                        object : Callback<String> {
                                            override fun onResponse(
                                                call: Call<String>,
                                                response: Response<String>
                                            ) {
                                                if(response.isSuccessful){
                                                    Toast.makeText(fragment.context, response as String, Toast.LENGTH_LONG).show()
                                                    dialog.dismiss()
                                                }else{
                                                    dialog.dismiss()
                                                    Toast.makeText(fragment.context,"No pudimos dar por cancelado el pedido, intentalo en un momento ", Toast.LENGTH_LONG).show()
                                                }
                                            }
                                            override fun onFailure(call: Call<String>, t: Throwable) {
                                                mList.remove(ItemViewModel)
                                                notifyItemRemoved(position)
                                                dialog.dismiss()
                                                Toast.makeText(fragment.context, "No pudimos dar por cancelado el pedido, intentalo en un momento ", Toast.LENGTH_LONG).show()
                                            }
                                        }
                                    )
                                }

                                true

                            }
                            else -> false
                        }
                    }
                })
                popup.show()
            }
        }




    }

    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val txtIdPedidoRes: TextView = itemView.findViewById(R.id.txtIdPedidoRes)
        val txtSobranteRest: TextView = itemView.findViewById(R.id.txtSobranteRest)
        val txtEstadoPedidosRest: TextView = itemView.findViewById(R.id.txtEstadoPedidosRest)
        val txtBancoRest: TextView = itemView.findViewById(R.id.txtBancoRest)
        val btnOption:ImageButton = itemView.findViewById(R.id.btn_option_row_pedidos_rest)
    }

}