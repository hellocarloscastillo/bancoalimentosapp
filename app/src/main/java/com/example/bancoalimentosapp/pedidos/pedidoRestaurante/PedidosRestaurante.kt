package com.example.bancoalimentosapp.pedidos.pedidoRestaurante

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.PedidosBancoFragmentBinding
import com.example.bancoalimentosapp.databinding.PedidosRestauranteFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.google.gson.Gson

class PedidosRestaurante : Fragment() {

    companion object {
        fun newInstance() = PedidosRestaurante()
    }

    private lateinit var viewModel: PedidosRestauranteViewModel
    private var _binding: PedidosRestauranteFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = PedidosRestauranteFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = PedidosRestauranteViewModel(activity, this)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var restSpinner = binding.spinnerRestPedidosRest
        var estadoSnniper = binding.snniperFiltroPedidosRest
        usuario = Gson().fromJson(
            sharedPreferences!!.getString("usuario", "{}"),
            UsuarioSesion::class.java
        )

        val listNotificaciones = activity?.findViewById<RecyclerView>(R.id.listPedidosRest)
        listNotificaciones?.layoutManager = LinearLayoutManager(activity)


        viewModel.cargarRestaurantes(usuario.id_usuario, restSpinner)




        restSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                vieww: View,
                position: Int,
                id: Long
            ) {
                viewModel.cargarNotificacionesPedidos(listNotificaciones!! ,viewModel.listRest[position].id_restaurantes)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        estadoSnniper.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                vieww: View,
                position: Int,
                id: Long
            ) {

                if(viewModel.listRest != null && viewModel.listRest.size > 0 ){
                    when (viewModel.estados.toList().get(position).first) {

                        "E" -> {
                            viewModel.cargarNotificacionesPedidos(listNotificaciones!! ,viewModel.listRest[restSpinner.selectedItemPosition].id_restaurantes)
                        }
                        "P" -> {
                            viewModel.buscarPendientesPorRestaurante(listNotificaciones!!, viewModel.listRest[restSpinner.selectedItemPosition].id_restaurantes)
                        }
                        "C" -> {
                            viewModel.buscarCanceladoPorRestaurante(listNotificaciones!!, viewModel.listRest[restSpinner.selectedItemPosition].id_restaurantes)
                        }
                        "T" -> {
                            viewModel.buscarTerminadoPorRestaurante(listNotificaciones!!, viewModel.listRest[restSpinner.selectedItemPosition].id_restaurantes)
                        }

                    }
                }


                //viewModel.cargarNotificacionesPedidos(listNotificaciones!! ,viewModel.listRest[position].id_restaurantes)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })


        viewModel.cargarEstados(estadoSnniper)
    }





}