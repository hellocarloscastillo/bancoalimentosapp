package com.example.bancoalimentosapp.pedidos.pedidoRestaurante

import android.R
import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.IdBancoAlimentos
import com.example.bancoalimentosapp.models.Notificacion
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.pedidos.pedidosBanco.ListPedidoBancoAdapter
import com.example.bancoalimentosapp.pedidos.pedidosBanco.RowPedidosBanco
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.services.NotificacionesSercvice
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.utils.EventRecyclerView
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PedidosRestauranteViewModel(var activity: Activity?, var fragment: Fragment ): EventRecyclerView {

    val notificacionesService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<NotificacionesSercvice>(NotificacionesSercvice::class.java)

    val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)

    var listRest: List<Restaurante> = mutableListOf()
    var listPedidos: MutableList<Notificacion> = mutableListOf()

    var estados = hashMapOf<String,String>("E" to "Ejecucion", "P" to "Pendiente", "T" to "Terminado", "C" to "Cancelado")

    fun cargarNotificacionesPedidos(listaPedidos : RecyclerView, idRest:Int){

        notificacionesService.notificacionesActivasPorRestaurante(
            idRest
        ).enqueue(
            object : Callback<List<Notificacion>> {
                override fun onResponse(
                    call: Call<List<Notificacion>>,
                    responseDos: Response<List<Notificacion>>
                ) {
                    if (responseDos.isSuccessful) {
                        var listAux = responseDos.body() as List<Notificacion>
                        if (listAux.size > 0) {
                            listPedidos = listAux.toMutableList()
                            var listReciclesView = mutableListOf<RowPedidoRest>()


                            listPedidos.map {
                                var nombreBanco = "No Disponible"
                                var nombreSobrante = "No Disponible"

                                bancoService.obtenerBancoPorId(it.id_bancos_alimentos).enqueue(
                                    object : Callback<Banco> {
                                        override fun onResponse(
                                            call: Call<Banco>,
                                            response: Response<Banco>
                                        ) {
                                            if(response.isSuccessful){
                                                var bancoAux = response.body() as Banco
                                                nombreBanco = bancoAux.nombre

                                                var rowAux = RowPedidoRest(it.id_notificaciones, it.id_sobrantes.toString(), estados.get(it.estado)!!  ,nombreBanco, it.id_bancos_alimentos)
                                                listReciclesView.add(rowAux)

                                                val adapter = ListPedidoRestAdapter(listReciclesView, fragment, this@PedidosRestauranteViewModel)
                                                listaPedidos!!.adapter = adapter
                                            }else{
                                                Toast.makeText(activity, "No pudimos acceder al servicio de bancos ", Toast.LENGTH_LONG).show()
                                            }
                                        }
                                        override fun onFailure(call: Call<Banco>, t: Throwable) {
                                            Toast.makeText(activity, "No pudimos acceder al servicio de bancos ", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                )


                            }


                        }else{
                            Toast.makeText(activity, "No tiene pedidos activos ", Toast.LENGTH_LONG).show()
                            val adapter = ListPedidoBancoAdapter(mutableListOf(), fragment)
                            listaPedidos!!.adapter = adapter
                        }

                    } else {
                        print("error cargando los pedidos disponibles ")
                        Toast.makeText(
                            activity,
                            "No pudimos cargar las notificaciones, por favor intenta mas tarde 1 id banco ${idRest}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onFailure(
                    call: Call<List<Notificacion>>,
                    t: Throwable
                ) {
                    print("error cargando los pedidos disponibles : ${t.message}")
                    Toast.makeText(
                        activity,
                        "No pudimos cargar las notificaciones, por favor intenta mas tarde id banco ${t.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )
    }

    fun buscarTerminadoPorRestaurante(listaPedidos : RecyclerView, idRest:Int){

        notificacionesService.buscarTerminadoPorRestaurante(
            idRest
        ).enqueue(
            object : Callback<List<Notificacion>> {
                override fun onResponse(
                    call: Call<List<Notificacion>>,
                    responseDos: Response<List<Notificacion>>
                ) {
                    if (responseDos.isSuccessful) {
                        var listAux = responseDos.body() as List<Notificacion>
                        if (listAux.size > 0) {
                            listPedidos = listAux.toMutableList()
                            var listReciclesView = mutableListOf<RowPedidoRest>()


                            listPedidos.map {
                                var nombreBanco = "No Disponible"
                                var nombreSobrante = "No Disponible"

                                bancoService.obtenerBancoPorId(it.id_bancos_alimentos).enqueue(
                                    object : Callback<Banco> {
                                        override fun onResponse(
                                            call: Call<Banco>,
                                            response: Response<Banco>
                                        ) {
                                            if(response.isSuccessful){
                                                var bancoAux = response.body() as Banco
                                                nombreBanco = bancoAux.nombre

                                                var rowAux = RowPedidoRest(it.id_notificaciones, it.id_sobrantes.toString(), estados.get(it.estado)!!  ,nombreBanco, it.id_bancos_alimentos)
                                                listReciclesView.add(rowAux)

                                                val adapter = ListPedidoRestAdapter(listReciclesView, fragment, this@PedidosRestauranteViewModel)
                                                listaPedidos!!.adapter = adapter
                                            }
                                        }
                                        override fun onFailure(call: Call<Banco>, t: Throwable) {
                                            Toast.makeText(activity, "No pudimos acceder al servicio de bancos ", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                )


                            }


                        }else{
                            Toast.makeText(activity, "No tiene pedidos terminados ", Toast.LENGTH_LONG).show()
                            val adapter = ListPedidoBancoAdapter(mutableListOf(), fragment)
                            listaPedidos!!.adapter = adapter
                        }

                    } else {
                        print("error cargando los pedidos disponibles ")
                        Toast.makeText(
                            activity,
                            "No pudimos cargar las notificaciones, por favor intenta mas tarde 1 id banco ${idRest}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onFailure(
                    call: Call<List<Notificacion>>,
                    t: Throwable
                ) {
                    print("error cargando los pedidos disponibles : ${t.message}")
                    Toast.makeText(
                        activity,
                        "No pudimos cargar las notificaciones, por favor intenta mas tarde id banco ${t.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )
    }

    fun buscarCanceladoPorRestaurante(listaPedidos : RecyclerView, idRest:Int){

        notificacionesService.buscarCanceladoPorRestaurante(
            idRest
        ).enqueue(
            object : Callback<List<Notificacion>> {
                override fun onResponse(
                    call: Call<List<Notificacion>>,
                    responseDos: Response<List<Notificacion>>
                ) {
                    if (responseDos.isSuccessful) {
                        var listAux = responseDos.body() as List<Notificacion>
                        if (listAux.size > 0) {
                            listPedidos = listAux.toMutableList()
                            var listReciclesView = mutableListOf<RowPedidoRest>()


                            listPedidos.map {
                                var nombreBanco = "No Disponible"
                                var nombreSobrante = "No Disponible"

                                bancoService.obtenerBancoPorId(it.id_bancos_alimentos).enqueue(
                                    object : Callback<Banco> {
                                        override fun onResponse(
                                            call: Call<Banco>,
                                            response: Response<Banco>
                                        ) {
                                            if(response.isSuccessful){
                                                var bancoAux = response.body() as Banco
                                                nombreBanco = bancoAux.nombre

                                                var rowAux = RowPedidoRest(it.id_notificaciones, it.id_sobrantes.toString(), estados.get(it.estado)!!  ,nombreBanco, it.id_bancos_alimentos)
                                                listReciclesView.add(rowAux)

                                                val adapter = ListPedidoRestAdapter(listReciclesView, fragment,this@PedidosRestauranteViewModel)
                                                listaPedidos!!.adapter = adapter
                                            }
                                        }
                                        override fun onFailure(call: Call<Banco>, t: Throwable) {
                                            Toast.makeText(activity, "No pudimos acceder al servicio de bancos ", Toast.LENGTH_LONG).show()
                                        }
                                    }
                                )


                            }


                        }else{
                            Toast.makeText(activity, "No tiene pedidos cancelados ", Toast.LENGTH_LONG).show()
                            val adapter = ListPedidoBancoAdapter(mutableListOf(), fragment)
                            listaPedidos!!.adapter = adapter
                        }

                    } else {
                        print("error cargando los pedidos disponibles ")
                        Toast.makeText(
                            activity,
                            "No pudimos cargar las notificaciones, por favor intenta mas tarde 1 id banco ${idRest}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onFailure(
                    call: Call<List<Notificacion>>,
                    t: Throwable
                ) {
                    print("error cargando los pedidos disponibles : ${t.message}")
                    Toast.makeText(
                        activity,
                        "No pudimos cargar las notificaciones, por favor intenta mas tarde id banco ${t.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )
    }

    fun buscarPendientesPorRestaurante(listaPedidos : RecyclerView, idRest:Int){

        notificacionesService.buscarPendientesPorRestaurante(
            idRest
        ).enqueue(
            object : Callback<List<Notificacion>> {
                override fun onResponse(
                    call: Call<List<Notificacion>>,
                    responseDos: Response<List<Notificacion>>
                ) {
                    if (responseDos.isSuccessful) {
                        var listAux = responseDos.body() as List<Notificacion>
                        if (listAux.size > 0) {
                            listPedidos = listAux.toMutableList()
                            var listReciclesView = mutableListOf<RowPedidoRest>()


                            listPedidos.map {
                                var nombreBanco = "No Disponible"
                                var nombreSobrante = "No Disponible"

                                var rowAux = RowPedidoRest(it.id_notificaciones, it.id_sobrantes.toString(), estados.get(it.estado)!!  ,nombreBanco, it.id_bancos_alimentos)
                                listReciclesView.add(rowAux)

                                val adapter = ListPedidoRestAdapter(listReciclesView, fragment, this@PedidosRestauranteViewModel)
                                listaPedidos!!.adapter = adapter

                            }


                        }else{
                            Toast.makeText(activity, "No tiene pedidos libres ", Toast.LENGTH_LONG).show()
                            val adapter = ListPedidoBancoAdapter(mutableListOf(), fragment)
                            listaPedidos!!.adapter = adapter
                        }

                    } else {
                        print("error cargando los pedidos disponibles ")
                        Toast.makeText(
                            activity,
                            "No pudimos cargar las notificaciones, por favor intenta mas tarde,  id banco ${idRest}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onFailure(
                    call: Call<List<Notificacion>>,
                    t: Throwable
                ) {
                    print("error cargando los pedidos disponibles : ${t.message}")
                    Toast.makeText(
                        activity,
                        "No pudimos cargar las notificaciones, por favor intenta mas tarde id banco ${t.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )
    }

    fun cargarRestaurantes(idUsr:Int, restauranteSpinner: Spinner){

        restauranteService.restaurantesPorUsuario(idUsr).enqueue(
            object : Callback<List<Restaurante>> {
                override fun onResponse(call: Call<List<Restaurante>>, response: Response<List<Restaurante>>) {
                    if (response.isSuccessful) {
                        listRest = response!!.body()!!
                        val listTitleBank: MutableList<String> = mutableListOf()
                        listRest.forEach {
                            listTitleBank.add(it.nombre)
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            restauranteSpinner.context,
                            R.layout.simple_spinner_item, listTitleBank
                        )

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        restauranteSpinner.setAdapter(adapter)
                    }else{
                        Toast.makeText(activity, "No pudimos cargar los restaurantes, por favor intenta mas tarde ", Toast.LENGTH_LONG).show()
                    }
                }
                override fun onFailure(call: Call<List<Restaurante>>, t: Throwable) {
                    Toast.makeText(activity, "No pudimos cargar  los restaurantes, por favor intenta mas tarde ", Toast.LENGTH_LONG).show()
                }
            }
        )

    }



    fun cargarEstados(spinnEstados: Spinner){
        var listEstados: MutableList<String> = mutableListOf()
        estados.mapValues { it
            listEstados.add(it.value)
        }

        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            spinnEstados.context,
            R.layout.simple_spinner_item, listEstados
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnEstados.setAdapter(adapter)
        spinnEstados.setSelection(3)
    }


    override fun onCLick(item: Any, pocicion: Int) {


    }


}