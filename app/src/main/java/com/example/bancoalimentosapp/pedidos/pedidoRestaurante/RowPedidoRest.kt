package com.example.bancoalimentosapp.pedidos.pedidoRestaurante

import java.io.Serializable

data class RowPedidoRest(
    var id:Int,
    var nombreSobrante:String,
    var estado:String,
    var banco:String,
    var id_banco:Int,
):Serializable
