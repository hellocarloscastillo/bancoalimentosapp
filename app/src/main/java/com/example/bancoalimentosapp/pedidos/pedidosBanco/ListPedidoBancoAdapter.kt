package com.example.bancoalimentosapp.pedidos.pedidosBanco

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.models.Notificacion

class ListPedidoBancoAdapter (val mList: MutableList<RowPedidosBanco>, val fragment: Fragment): RecyclerView.Adapter<ListPedidoBancoAdapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pedidoabanco, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemViewModel = mList[position]

        // sets the image to the imageview from our itemHolder class

        holder.id_notificaciones.text = ItemViewModel.id.toString()
        holder.estadoNotiBancoRow.text = ItemViewModel.estado
        holder.bancoRowNoti.text = ItemViewModel.banco
        holder.sobranteRowNotiBanco.text = ItemViewModel.sobrante
        holder.restRowNotiBanco.text = ItemViewModel.restaurante

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val id_notificaciones: TextView = itemView.findViewById(R.id.id_notificaciones)
        val estadoNotiBancoRow: TextView = itemView.findViewById(R.id.estadoNotiBancoRow)
        val bancoRowNoti: TextView = itemView.findViewById(R.id.bancoRowNoti)
        val restRowNotiBanco: TextView = itemView.findViewById(R.id.restRowNotiBanco)
        val sobranteRowNotiBanco: TextView = itemView.findViewById(R.id.sobranteRowNotiBanco)


    }

}