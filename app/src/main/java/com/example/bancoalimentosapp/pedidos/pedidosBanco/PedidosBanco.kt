package com.example.bancoalimentosapp.pedidos.pedidosBanco

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.DetalleSobranteFragmentBinding
import com.example.bancoalimentosapp.databinding.PedidosBancoFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.sobrasDisponibles.detalleSobrante.DetalleSobranteViewModel
import com.google.gson.Gson

class PedidosBanco : Fragment() {

    companion object {
        fun newInstance() = PedidosBanco()
    }

    private lateinit var viewModel: PedidosBancoViewModel
    private var _binding: PedidosBancoFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = PedidosBancoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = PedidosBancoViewModel(activity, this)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        var bancoSnnipe = binding.snnipBancosPedidos
        var estadoSnniper = binding.spinnerFiltroPedidosBanco
        usuario = Gson().fromJson(
            sharedPreferences!!.getString("usuario", "{}"),
            UsuarioSesion::class.java
        )

        val listNotificaciones = activity?.findViewById<RecyclerView>(R.id.listPedidosBanco)
        listNotificaciones?.layoutManager = LinearLayoutManager(activity)


        viewModel.cargarBancos(usuario.id_usuario, bancoSnnipe)

        bancoSnnipe.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
         override fun onItemSelected(
             parent: AdapterView<*>?,
             vieww: View,
             position: Int,
             id: Long
         ) {
             viewModel.cargarNotificacionesPedidos(listNotificaciones!! ,viewModel.listBank[position].id_bancos_alimentos)
         }

         override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        viewModel.cargarEstados(estadoSnniper)
    }

}