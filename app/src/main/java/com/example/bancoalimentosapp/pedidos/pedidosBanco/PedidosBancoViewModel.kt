package com.example.bancoalimentosapp.pedidos.pedidosBanco

import android.R
import android.app.Activity
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.forEach
import androidx.core.view.size
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.Bancos.ListBancoAdapter
import com.example.bancoalimentosapp.empleados.EmployeRow
import com.example.bancoalimentosapp.empleados.ListEmployeAdapter
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.IdBancoAlimentos
import com.example.bancoalimentosapp.models.Notificacion
import com.example.bancoalimentosapp.services.BancoService
import com.example.bancoalimentosapp.services.NotificacionesSercvice
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PedidosBancoViewModel(var activity: Activity?,var fragment: Fragment) {

    val notificacionesService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<NotificacionesSercvice>(NotificacionesSercvice::class.java)

    val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    var listBank: List<Banco> = mutableListOf()
    var listPedidos: MutableList<Notificacion> = mutableListOf()

    var estados = hashMapOf<String,String>("E" to "Ejecucion", "P" to "Pendiente", "C" to "Cancelado", "T" to "Terminado")

    fun cargarNotificacionesPedidos(listaPedidos : RecyclerView, idBanco:Int){

        notificacionesService.notificacionesActivasPorBanco(
            IdBancoAlimentos(
                idBanco
            )
        ).enqueue(
            object : Callback<List<Notificacion>> {
                override fun onResponse(
                    call: Call<List<Notificacion>>,
                    responseDos: Response<List<Notificacion>>
                ) {
                    if (responseDos.isSuccessful) {
                        var listAux = responseDos.body() as List<Notificacion>
                        if (listAux.size > 0) {
                            listPedidos = listAux.toMutableList()
                            var listReciclesView = mutableListOf<RowPedidosBanco>()
                            listPedidos.map {
                                var nombreBank:String = "No Disponible"
                                listBank.mapIndexed { index, bancoAux ->
                                    if(bancoAux.id_bancos_alimentos == it.id_bancos_alimentos){
                                        nombreBank = bancoAux.nombre
                                    }
                                }
                                var row = RowPedidosBanco(it.id_notificaciones, estados.get(it.estado)!!, nombreBank, "No disponible", it.id_sobrantes.toString()   )
                                listReciclesView.add(row)
                            }
                            val adapter = ListPedidoBancoAdapter(listReciclesView, fragment)
                            listaPedidos!!.adapter = adapter
                        }else{
                            Toast.makeText(activity, "No tiene pedidos activos ", Toast.LENGTH_LONG).show()
                            val adapter = ListPedidoBancoAdapter(mutableListOf(), fragment)
                            listaPedidos!!.adapter = adapter
                        }

                    } else {
                        print("error cargando los pedidos disponibles ")
                        Toast.makeText(
                            activity,
                            "No pudimos cargar las notificaciones, por favor intenta mas tarde 1 id banco ${idBanco}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onFailure(
                    call: Call<List<Notificacion>>,
                    t: Throwable
                ) {
                    print("error cargando los pedidos disponibles : ${t.message}")
                    Toast.makeText(
                        activity,
                        "No pudimos cargar las notificaciones, por favor intenta mas tarde id banco ${t.message}",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        )
    }

    fun cargarBancos(idUsr:Int, bancos: Spinner){

        bancoService.listarBnacosPorUsuario(idUsr).enqueue(
            object :Callback<List<Banco>> {
                override fun onResponse(call: Call<List<Banco>>, response: Response<List<Banco>>) {
                    if (response.isSuccessful) {
                        listBank = response!!.body()!!
                        val listTitleBank: MutableList<String> = mutableListOf()
                        listBank.forEach {
                            listTitleBank.add(it.nombre)
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            bancos.context,
                            R.layout.simple_spinner_item, listTitleBank
                        )

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        bancos.setAdapter(adapter)
                    }else{
                        Toast.makeText(activity, "No pudimos cargar las bancos, por favor intenta mas tarde ", Toast.LENGTH_LONG).show()
                    }
                }
                override fun onFailure(call: Call<List<Banco>>, t: Throwable) {
                    Toast.makeText(activity, "No pudimos cargar las bancos, por favor intenta mas tarde ", Toast.LENGTH_LONG).show()
                }
            }
        )

    }


    fun cargarEstados(spinnEstados: Spinner){
        var listEstados: MutableList<String> = mutableListOf()
        estados.get("E")?.let { listEstados.add(it) }

        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            spinnEstados.context,
            R.layout.simple_spinner_item, listEstados
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnEstados.setAdapter(adapter)
        spinnEstados.setSelection(0)
    }


    fun concatenate(vararg lists: List<Notificacion>): List<Notificacion> {
        return listOf(*lists).flatten()
    }

}