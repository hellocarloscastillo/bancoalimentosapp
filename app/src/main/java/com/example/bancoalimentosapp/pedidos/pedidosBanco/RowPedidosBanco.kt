package com.example.bancoalimentosapp.pedidos.pedidosBanco

import java.io.Serializable

data class RowPedidosBanco(
    var id: Int,
    var estado : String,
    var banco: String,
    var restaurante: String,
    var sobrante: String
):Serializable
