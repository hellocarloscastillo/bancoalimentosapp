package com.example.bancoalimentosapp.registroRestaurante

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.RegistroRestauranteFragmentBinding
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.RestauranteRow

class RegistroRestaurante : Fragment() {

    companion object {
        fun newInstance() = RegistroRestaurante()
    }

    private lateinit var viewModel: RegistroRestauranteViewModel

    private var _binding: RegistroRestauranteFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RegistroRestauranteFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = RegistroRestauranteViewModel(activity, this)

        var restauranteAux = arguments?.getSerializable("restauranteAux")

        val btnResgistrorest = binding.btnResgistrorest
        val btnCancel = binding.btnCancelarRest

        val restName: EditText = binding.restName
        val dirRest: EditText = binding.dirRest
        val telRest: EditText = binding.telRest
        val closeRest: EditText = binding.closeRest
        val snipTipoRest: Spinner = binding.snipTipoRest

        viewModel.cargarTipoRestaurante(view, snipTipoRest)

        if(restauranteAux == null){
            initNewRest(snipTipoRest,btnResgistrorest,restName,dirRest,telRest,closeRest,btnCancel)
        }else{
            initUpdateRest(snipTipoRest,btnResgistrorest,restName,dirRest,telRest,closeRest,btnCancel, restauranteAux as RestauranteRow)
        }


    }


    fun initNewRest( snipTipoRest: Spinner, btnResgistrorest:Button,restName: EditText,dirRest: EditText,telRest: EditText,closeRest: EditText, btnCancel:Button){

        btnResgistrorest.setOnClickListener {
            viewModel.createRestaurant(restName,dirRest,telRest,closeRest, snipTipoRest)
        }
        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.loginFragment )
        }
    }

    fun initUpdateRest( snipTipoRest: Spinner, btnResgistrorest:Button,restName: TextView,dirRest: TextView,telRest: TextView,closeRest: TextView, btnCancel:Button, restAux: RestauranteRow){
        binding.btnResgistrorest.text = "Actualizar"
        restName.text = restAux.nombre
        dirRest.text = restAux.dirección
        telRest.text = restAux.telefono.toString()
        closeRest.text = restAux.horarios

        btnResgistrorest.setOnClickListener {
            viewModel.actualizarRestaurant(restAux,restName,dirRest,telRest,closeRest, snipTipoRest)
        }
        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.listaRestaurante )
        }
    }

}