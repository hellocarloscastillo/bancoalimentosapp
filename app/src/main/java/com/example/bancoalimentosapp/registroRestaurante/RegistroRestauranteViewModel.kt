package com.example.bancoalimentosapp.registroRestaurante

import android.R
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.bancoalimentosapp.empleados.nuevoEmpleado.validateResponse
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.Empleado
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.models.TipoRestaurante
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.services.TipoRestauranteService
import com.example.bancoalimentosapp.utils.Settings
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.RestauranteRow


class RegistroRestauranteViewModel(val activity: Activity?, val fragment: Fragment) {

    private val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)

    private val tipoRestauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<TipoRestauranteService>(TipoRestauranteService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)
    lateinit var listTipoRestaurante: List<TipoRestaurante>

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    fun cargarTipoRestaurante(view: View, bancos: Spinner){
        tipoRestauranteService.listarTipoRestaurante().enqueue(
            object : Callback<List<TipoRestaurante>> {
                override fun onResponse(
                    call: Call<List<TipoRestaurante>>,
                    response: Response<List<TipoRestaurante>>
                ) {
                    listTipoRestaurante = response!!.body()!!
                    val listTitleBank: MutableList<String> = mutableListOf()
                    listTipoRestaurante.forEach {
                        listTitleBank.add(it.nombre)
                    }

                    val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                        view.context,
                        R.layout.simple_spinner_item, listTitleBank
                    )

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    bancos.setAdapter(adapter)
                }

                override fun onFailure(call: Call<List<TipoRestaurante>>?, t: Throwable?) {
                    TODO("Not yet implemented")
                }

            }
        )
    }

    fun createRestaurant(restName: EditText,dirRest: EditText,telRest: EditText,closeRest: EditText,snipTipoRest: Spinner){

        var newRestaurant = Restaurante(-1,
            restName.text.toString(),
            dirRest.text.toString(),
            "-1;-1",
            if (telRest.text.toString().equals("")) -1 else telRest.text.toString().toInt(),
            closeRest.text.toString() ,
            usuarioSession.id_usuario,
            listTipoRestaurante.get(snipTipoRest.selectedItemPosition).id_tipo_restaurante
        )
        val resultSave = validateRest(newRestaurant)
        println("nuevo rest : " + newRestaurant.toString())
        if(resultSave.success){
            restauranteService.guardarRestaurante(resultSave.dato as Restaurante).enqueue(
                object : Callback<Restaurante>{
                    override fun onResponse(call: Call<Restaurante>, response: Response<Restaurante>) {
                        if(response!!.isSuccessful){
                            Toast.makeText(fragment.context, "Restaurante Resgistrado con Exito!", Toast.LENGTH_LONG).show()
                            fragment.findNavController().navigate(com.example.bancoalimentosapp.R.id.listaRestaurante  )
                        }else{
                            Toast.makeText(fragment.context, "No pudimos registrar el restaurante, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                        }

                    }

                    override fun onFailure(call: Call<Restaurante>, t: Throwable) {
                        Snackbar.make(restName, "No fue posible el registro, por favor intenta mas tarde", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                }
            )
        }
        else{
            Snackbar.make(restName, resultSave.dato as String, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun actualizarRestaurant(restAux: RestauranteRow, restName: TextView, dirRest: TextView, telRest: TextView, closeRest: TextView, snipTipoRest: Spinner){

        var newRestaurant = Restaurante(restAux.id_restaurantes,
            restName.text.toString(),
            dirRest.text.toString(),
            restAux.geolocalizacion,
            if (telRest.text.toString().equals("")) -1 else telRest.text.toString().toInt(),
            closeRest.text.toString() ,
            usuarioSession.id_usuario,
            listTipoRestaurante.get(snipTipoRest.selectedItemPosition).id_tipo_restaurante
        )
        val resultSave = validateRest(newRestaurant)
        if(resultSave.success){
            restauranteService.actualizarRestaurante(resultSave.dato as Restaurante).enqueue(
                object : Callback<Restaurante>{
                    override fun onResponse(call: Call<Restaurante>, response: Response<Restaurante>) {
                        if(response!!.isSuccessful){
                            Toast.makeText(fragment.context, "Restaurante Actualizado con Exito!", Toast.LENGTH_LONG).show()
                            fragment.findNavController().navigate(com.example.bancoalimentosapp.R.id.listaRestaurante  )
                        }else{
                            Toast.makeText(fragment.context, "No pudimos actualizar el restaurante, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<Restaurante>, t: Throwable) {
                        Snackbar.make(restName, "No fue posible actualizar, por favor intenta mas tarde", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                }
            )
        }
        else{
            Snackbar.make(restName, resultSave.dato as String, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

    fun validateRest(newRest: Restaurante): validateResponse {
        if(newRest.nombre.equals("") || newRest.nombre.length < 3){
            return validateResponse(false, "Nombre incorrecto")
        }
        if(newRest.dirección.equals("") || newRest.dirección.length < 5){
            return validateResponse(false, "La direccion es muy corta ")
        }
        if(newRest.telefono < 99999 || newRest.telefono == null || newRest.telefono > 9999999999){
            return validateResponse(false, "Telefono incorrecto")
        }
        if(newRest.horarios == null || newRest.horarios.toString().equals("") ){
            return validateResponse(false, "Hora de cierre no estipulada")
        }
        if(newRest.id_tipo_restaurante == null){
            return validateResponse(false, "Tipo de restaurante incorrecto, no hay un banco seleccionado")
        }
        return validateResponse(true, newRest)
    }


}