package com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.Bancos.BancoRow
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListRestaurantAdapter (val mList: MutableList<RestauranteRow>, val contex: Context, private val fragment: Fragment): RecyclerView.Adapter<ListRestaurantAdapter.ViewHolder>() {

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)
    lateinit var dialog: Dialog;
    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_restaurante, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val ItemViewModel = mList[position]
        dialog = Dialog(contex)
        // sets the image to the imageview from our itemHolder class

        holder.idRest.text = ItemViewModel.id_restaurantes.toString()
        holder.nameRest.text =  ItemViewModel.nombre
        holder.dirRest.text =   ItemViewModel.dirección



        holder.btnOptionRestRow.setOnClickListener { //creating a popup menu
            val popup = PopupMenu(contex, holder.btnOptionRestRow)
            //inflating menu from xml resource
            popup.inflate(R.menu.option_row_restaurantes)
            //adding click listener
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    return when (item.getItemId()) {
                        R.id.editRest -> {
                            var restauranteAux = Bundle()
                            restauranteAux.putSerializable("restauranteAux", ItemViewModel)
                            fragment.findNavController().navigate(R.id.registroRestaurante, restauranteAux )
                            true
                        }

                        R.id.delRest -> {
                            openOptionRegister(ItemViewModel, position)
                            true
                        }

                        R.id.ubicationRestOptionRow ->  //handle menu2 click
                        {
                            val bundle = Bundle()
                            bundle.putInt("idRestaurante", ItemViewModel.id_restaurantes)
                            fragment.findNavController().navigate(R.id.registerUbication, bundle )
                            true
                        }
                        else -> false
                    }
                }
            })
            //displaying the popup
            popup.show()
        }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val idRest: TextView = itemView.findViewById(R.id.idRest)
        val nameRest: TextView = itemView.findViewById(R.id.nameRest)
        val dirRest: TextView = itemView.findViewById(R.id.dirRest)
        val btnOptionRestRow: ImageButton = itemView.findViewById(R.id.btnOptionRestRow)
    }


    private fun openOptionRegister(ItemViewModel: RestauranteRow, position: Int){
        dialog.setContentView(R.layout.mensaje_confirmacion)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        var btnYes = dialog.findViewById<Button>(R.id.btnYesDialogConfirm)
        var btnNo = dialog.findViewById<Button>(R.id.btnNoDialogConfirm)
        var text = dialog.findViewById<TextView>(R.id.textDialogCOnfirm)
        text.text = "Seguro que sea eliminar el restaurante ${ItemViewModel.nombre}"
        btnYes.setOnClickListener {
            restauranteService.eliminarRestaurante(ItemViewModel).enqueue(
                object :Callback<Any> {
                    override fun onResponse(
                        call: Call<Any>,
                        response: Response<Any>
                    ) {
                        if(response.isSuccessful){
                            mList.remove(ItemViewModel)
                            notifyItemRemoved(position)
                            Toast.makeText(contex, "Restaurante eliminado", Toast.LENGTH_LONG).show()
                        }else{
                            Toast.makeText(contex, "No se pudo eliminar el Restaurante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<Any>, t: Throwable) {
                        mList.remove(ItemViewModel)
                        notifyItemRemoved(position)
                        Toast.makeText(contex, "No se pudo eliminar el Restaurante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )

        }

        btnNo.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

}