package com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.Vehiculos.CarRow
import com.example.bancoalimentosapp.Vehiculos.ListCarAdapter
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.models.Vehiculo
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListaRestauranteViewModel(val activity: Activity?, val fragment: Fragment) {

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    fun listarRestaurantes(listaCar: RecyclerView){
        restauranteService.restaurantesPorUsuario(usuarioSession.id_usuario).enqueue(
            object: Callback<List<Restaurante>> {
                override fun onResponse(
                    call: Call<List<Restaurante>>,
                    response: Response<List<Restaurante>>
                ) {
                    if(response!!.isSuccessful){
                        val listrest: MutableList<RestauranteRow> = mutableListOf()
                        response!!.body()!!.forEach {
                            listrest.add( RestauranteRow(it.id_restaurantes, it.nombre, it.dirección, it.id_tipo_restaurante.toString(), it.horarios, it.telefono, it.geolocalizacion, it.id_usuario ))
                        }
                        val adapter = ListRestaurantAdapter(listrest, listaCar.context, fragment )
                        listaCar!!.adapter = adapter
                    }else{
                        Toast.makeText(activity, "NO se pudieron cargar los restaurantes, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<Restaurante>>, t: Throwable) {
                    Toast.makeText(activity, "NO se pudieron cargar los restaurantes, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                }
            }
        )
    }
}