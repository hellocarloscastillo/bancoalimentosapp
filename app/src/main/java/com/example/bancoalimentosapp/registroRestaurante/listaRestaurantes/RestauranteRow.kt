package com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes

import java.io.Serializable

data class RestauranteRow(
    var id_restaurantes:Int,
    var nombre: String,
    var dirección: String,
    var id_tipo_restaurante: String,
    var horarios:String,
    var telefono: Int,
    var geolocalizacion:String,
    var id_usuario: Int
): Serializable
