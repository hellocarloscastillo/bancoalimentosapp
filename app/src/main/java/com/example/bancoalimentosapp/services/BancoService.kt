package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.Bancos.BancoRow
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.Bancos.registroBanco.NewBanco
import retrofit2.Call
import retrofit2.http.*

interface BancoService {

    @POST("banco/")
    fun guardarBanco(@Body newBanco: NewBanco): Call<Banco>

    @PUT("banco/")
    fun actualizarBanco(@Body newBanco: BancoRow): Call<Banco>

    @GET("banco/porUsuario/{id_usuario}")
    fun listarBnacosPorUsuario(@Path("id_usuario") id_usuario:Int): Call<List<Banco>>

    @GET("banco/bancoPorId/{id_banco}")
    fun obtenerBancoPorId(@Path("id_banco") id_banco:Int): Call<Banco>

    @HTTP(method = "DELETE", path = "banco/", hasBody = true)
    fun eliminarBanco(@Body banco: BancoRow): Call<Any>
}