package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.Bancos.BancoRow
import com.example.bancoalimentosapp.empleados.EmployeRow
import com.example.bancoalimentosapp.models.Empleado
import retrofit2.Call
import retrofit2.http.*

interface EmpleadoService {

    @POST("empleados/")
    fun guardarEmpleado(@Body empleado: Empleado): Call<Empleado>

    @PUT("empleados/")
    fun actualizarEmpleado(@Body empleado: Empleado): Call<Empleado>

    @GET("empleados/")
    fun listarEmpleados(): Call<List<Empleado>>

    @GET("empleados/empleadoPorBanco/{id_usuario}")
    fun empleadosPorUsuario(@Path("id_usuario")id_usuario:Int): Call<List<Empleado>>

    @HTTP(method = "DELETE", path = "empleados/", hasBody = true)
    fun eliminarEmpleado(@Body empleado: EmployeRow): Call<Any>
}