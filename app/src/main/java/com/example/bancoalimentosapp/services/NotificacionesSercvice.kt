package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.Vehiculos.CarRow
import com.example.bancoalimentosapp.models.AsignarBancoNotificacion
import com.example.bancoalimentosapp.models.IdBancoAlimentos
import com.example.bancoalimentosapp.models.NotiPorSobrante
import com.example.bancoalimentosapp.models.Notificacion
import retrofit2.Call
import retrofit2.http.*

interface NotificacionesSercvice {

    @GET("notificaciones/pendienteAsignar")
    fun buscarPendientes():Call<List<Notificacion>>

    @POST("notificaciones/asignar")
    fun asignarBAncoANotificacion(@Body bancoNotificacion:AsignarBancoNotificacion):Call<String>

    @POST("notificaciones/buscarAceptadasPorBancos")
    fun notificacionesActivasPorBanco(    @Body idBAnco:IdBancoAlimentos):Call<List<Notificacion>>

    @GET("notificaciones/todasEjecucionPorRestaurantes/{idRestaurante}")
    fun notificacionesActivasPorRestaurante( @Path("idRestaurante") idRestaurante:Int):Call<List<Notificacion>>

    @GET("notificaciones/todasPentiendesPorRestaurantes/{idRestaurante}")
    fun buscarPendientesPorRestaurante(@Path("idRestaurante") idRestaurante:Int):Call<List<Notificacion>>

    @GET("notificaciones/todasTerminadoPorRestaurantes/{idRestaurante}")
    fun buscarTerminadoPorRestaurante(@Path("idRestaurante") idRestaurante:Int):Call<List<Notificacion>>

    @GET("notificaciones/todasCaneladoPorRestaurantes/{idRestaurante}")
    fun buscarCanceladoPorRestaurante(@Path("idRestaurante") idRestaurante:Int):Call<List<Notificacion>>

    //cambios de estado

    @PUT("notificaciones/termiado")
    fun cambioEstadoTerminado(@Body notificacion:Notificacion): Call<String>

    @PUT("notificaciones/termiado")
    fun cambioEstadoCancelado(@Body notificacion:Notificacion): Call<String>

    @GET("notificaciones/buscarPorIdSobrante/{id_sobrante}")
    fun buscarNotiPorIdSobrante(@Path("id_sobrante") id_sobrante:Int):Call<List<NotiPorSobrante>>

}