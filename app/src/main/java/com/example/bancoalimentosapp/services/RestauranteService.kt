package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.Bancos.registroBanco.NewBanco
import com.example.bancoalimentosapp.empleados.EmployeRow
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.RestauranteRow
import retrofit2.Call
import retrofit2.http.*

interface RestauranteService {

    @POST("restaurante/")
    fun guardarRestaurante(@Body newRest: Restaurante): Call<Restaurante>

    @GET("restaurante/buscarPorUsuario/{id_usuario}")
    fun restaurantesPorUsuario(@Path("id_usuario") id_usuario:Int):Call<List<Restaurante>>

    @GET("restaurante/{id_restaurante}")
    fun obtenerRestaurantePorId(@Path("id_restaurante") id_restaurante:Int):Call<Restaurante>

    @PUT("restaurante/")
    fun actualizarRestaurante(@Body newRest: Restaurante): Call<Restaurante>

    @HTTP(method = "DELETE", path = "restaurante/", hasBody = true)
    fun eliminarRestaurante(@Body restaurante: RestauranteRow): Call<Any>
}