package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.models.*
import com.example.bancoalimentosapp.pedidos.pedidoRestaurante.RowPedidoRest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface SobrantesServices {

    @GET("sobrantes/")
    fun sobrasDisponibles(): Call<List<SobrasDisponibles>>

    @POST("sobrantes/")
    fun guardarSobrante(@Body sobrante:SobrantePost): Call<SobranteGet>

    @POST("sobraRestaurante/")
    fun guardarSobraRestaurante(@Body sobraRestaurante:SobraRestaurante): Call<SobraRestaurante>

    @GET("sobrantes/buscarTodosPorIdUsuario/{id_usuario}")
    fun obtenerListaDonantes(@Path("id_usuario") id_usuario:Int): Call<List<RowDonanteRest>>
}