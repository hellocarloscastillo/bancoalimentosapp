package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.models.TipoRestaurante
import retrofit2.Call
import retrofit2.http.GET

interface TipoRestauranteService {

    @GET("tipoRestaurantes/")
    fun listarTipoRestaurante(): Call<List<TipoRestaurante>>

}