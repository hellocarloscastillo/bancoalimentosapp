package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.models.TipoRestaurante
import com.example.bancoalimentosapp.models.TipoSobrante
import retrofit2.Call
import retrofit2.http.GET

interface TipoSobranteService {

    @GET("tipoSobras/")
    fun listarTipoRestaurante(): Call<List<TipoSobrante>>
}