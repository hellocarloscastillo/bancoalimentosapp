package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.usuarios.models.NuevoUsuario
import retrofit2.Call
import retrofit2.http.*

interface UsuarioService {

    @GET("usuarios/{documento}/{contrasena}")
    fun login(@Path("documento") documento: String, @Path("contrasena") contrasena: String): Call<UsuarioSesion>

    @POST("usuarios/")
    fun guardarUsuario(@Body usuario: NuevoUsuario): Call<UsuarioSesion>

    @GET("usuarios/{documento}")
    fun consultarPorDocumento(@Path("documento") documento: String ): Call<UsuarioSesion>
}