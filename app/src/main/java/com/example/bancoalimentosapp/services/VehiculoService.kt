package com.example.bancoalimentosapp.services

import com.example.bancoalimentosapp.Vehiculos.CarRow
import com.example.bancoalimentosapp.models.Vehiculo
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.RestauranteRow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.*

interface VehiculoService {

    @GET("vehiculo/banco/{id_banco}")
    fun vehiculosPorBanco(@Path("id_banco") id_banco:Int): Call<List<Vehiculo>>

    @GET("vehiculo/")
    fun vehiculosPorUsuario(): Call<List<Vehiculo>>

    @POST("vehiculo/")
    fun guardarVehiculo(@Body vehiculo:Vehiculo): Call<Vehiculo>

    @PUT("vehiculo/")
    fun actualizarVehiculo(@Body vehiculo:Vehiculo): Call<Vehiculo>

    @HTTP(method = "DELETE", path = "vehiculo/", hasBody = true)
    fun eliminarVehiculo(@Body vehiculo: CarRow): Call<Any>
}