package com.example.bancoalimentosapp.sobrasDisponibles

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.ListRestaurantAdapter
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.RestauranteRow
import com.example.bancoalimentosapp.services.EmpleadoService
import com.example.bancoalimentosapp.services.SobrantesServices
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListSobrasDisponiblesAdapter (val mList: MutableList<SobrasDisponiblesRow>, val fragment: Fragment): RecyclerView.Adapter<ListSobrasDisponiblesAdapter.ViewHolder>() {

    private val sobrantesServices = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<SobrantesServices>(SobrantesServices::class.java)

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_sobras_disponibles, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemViewModel = mList[position]

        // sets the image to the imageview from our itemHolder class

        holder.nombreSobra.text = ItemViewModel.nombre
        holder.cantidadSobra.text = ItemViewModel.cantidad.toString() + " Libras"
        holder.tipoSobra.text = ItemViewModel.tipoSobra

        holder.btnTomarSobra.setOnClickListener { //creating a popup menu
            val dat = Bundle()
            dat.putSerializable("notificacion",ItemViewModel)
            print("notificacion : ${ItemViewModel.id_restaurantes}")
            fragment.findNavController().navigate(R.id.detalleSobrante, dat)
        }

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val nombreSobra: TextView = itemView.findViewById(R.id.nombreSobra)
        val cantidadSobra: TextView = itemView.findViewById(R.id.cantidadSobra)
        val tipoSobra: TextView = itemView.findViewById(R.id.tipoSobra)

        val btnTomarSobra: Button = itemView.findViewById(R.id.btnTomarSobra)
    }

}