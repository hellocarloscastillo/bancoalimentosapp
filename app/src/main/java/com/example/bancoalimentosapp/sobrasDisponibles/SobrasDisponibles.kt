package com.example.bancoalimentosapp.sobrasDisponibles

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.ListaRestauranteFragmentBinding
import com.example.bancoalimentosapp.databinding.SobrasDisponiblesFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.ListaRestauranteViewModel
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson

class SobrasDisponibles : Fragment() {

    companion object {
        fun newInstance() = SobrasDisponibles()
    }

    private lateinit var viewModel: SobrasDisponiblesViewModel

    private var _binding: SobrasDisponiblesFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SobrasDisponiblesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = SobrasDisponiblesViewModel(activity, this)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)
        var appbar = activity?.findViewById(R.id.appbarlayout) as AppBarLayout
        appbar.visibility = View.VISIBLE
        appbar.getLayoutParams().height= ViewGroup.LayoutParams.WRAP_CONTENT;

        var nameUsr: TextView = activity?.findViewById(R.id.nameUsrMenu) as TextView
        nameUsr.text = usuario.nombre

        var emailUsr: TextView = activity?.findViewById(R.id.emailUsrMenu) as TextView
        emailUsr.text = usuario.correo

        val recyclerListCar = binding.listSobrasDisponibles
        recyclerListCar?.layoutManager = LinearLayoutManager(activity)

        viewModel.listarRestaurantes(recyclerListCar)




    }

}