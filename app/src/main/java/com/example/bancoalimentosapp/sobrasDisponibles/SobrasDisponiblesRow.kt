package com.example.bancoalimentosapp.sobrasDisponibles

import java.io.Serializable

data class SobrasDisponiblesRow(
    val id_notificaciones:Int,
    val estado:String,
    val id_restaurantes:Int,
    val nombre:String,
    val cantidad: Int,
    val tipoSobra:String,
):Serializable
