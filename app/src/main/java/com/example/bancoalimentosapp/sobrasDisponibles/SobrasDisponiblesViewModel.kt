package com.example.bancoalimentosapp.sobrasDisponibles

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.*
import com.example.bancoalimentosapp.models.SobrasDisponibles
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.ListRestaurantAdapter
import com.example.bancoalimentosapp.registroRestaurante.listaRestaurantes.RestauranteRow
import com.example.bancoalimentosapp.services.NotificacionesSercvice
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.services.SobrantesServices
import com.example.bancoalimentosapp.services.TipoSobranteService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SobrasDisponiblesViewModel (val activity: Activity?, val fragment:Fragment) {
    val sobrasService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<SobrantesServices>(SobrantesServices::class.java)

    val tipoSobraService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<TipoSobranteService>(TipoSobranteService::class.java)

    val notificacionesService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<NotificacionesSercvice>(NotificacionesSercvice::class.java)

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<NotificacionesSercvice>(NotificacionesSercvice::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    var listTipoSobras: List<TipoSobrante> = mutableListOf()

    var listSobrante:List<SobrasDisponibles> = mutableListOf()

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)

    fun listarRestaurantes(listaCar: RecyclerView){

        tipoSobraService.listarTipoRestaurante().enqueue(
            object : Callback<List<TipoSobrante>>{
                override fun onResponse(
                    call: Call<List<TipoSobrante>>,
                    response: Response<List<TipoSobrante>>
                ) {
                    if(response.isSuccessful){
                        listTipoSobras = response.body() as List<TipoSobrante>

                        sobrasService.sobrasDisponibles().enqueue(
                            object: Callback<List<SobrasDisponibles>> {
                                override fun onResponse(
                                    call: Call<List<SobrasDisponibles>>,
                                    response: Response<List<SobrasDisponibles>>
                                ) {
                                    if(response!!.isSuccessful){
                                        listSobrante = response.body() as List<SobrasDisponibles>

                                        notificacionesService.buscarPendientes().enqueue(
                                            object : Callback<List<Notificacion>> {
                                                override fun onResponse(
                                                    call: Call<List<Notificacion>>,
                                                    response: Response<List<Notificacion>>
                                                ) {
                                                    if (response.isSuccessful) {
                                                        val listrest: MutableList<SobrasDisponiblesRow> = mutableListOf()

                                                        if(response.body()!!.size < 0){
                                                            Toast.makeText(listaCar.context, "No hay sobras disponibles", Toast.LENGTH_LONG).show()
                                                        }
                                                        response!!.body()!!.forEach {
                                                            var nombreTipo = "No Disponible"
                                                            lateinit var sobranteAux: SobrasDisponibles

                                                            listSobrante.mapIndexed { index, sobrante ->
                                                                if(it.id_sobrantes ==  sobrante.id_sobrantes){
                                                                    sobranteAux = sobrante

                                                                }
                                                            }

                                                            listTipoSobras.map {  tipoSobrante ->
                                                                if(tipoSobrante.id_tipo_sobrante == sobranteAux.id_tipo_sobrante){
                                                                    nombreTipo = tipoSobrante.nombre
                                                                }
                                                            }

                                                            listrest.add( SobrasDisponiblesRow(it.id_notificaciones,it.estado, it.id_restaurantes , sobranteAux.nombre, sobranteAux.cantidad, nombreTipo ))

                                                        }
                                                        val adapter = ListSobrasDisponiblesAdapter(listrest, fragment)
                                                        listaCar!!.adapter = adapter
                                                    }else{
                                                        Toast.makeText(listaCar.context, "No pudimos cargar las sobras disponibles, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                                                    }
                                                }

                                                override fun onFailure(
                                                    call: Call<List<Notificacion>>,
                                                    t: Throwable
                                                ) {
                                                    Toast.makeText(listaCar.context, "No pudimos cargar las sobras disponibles, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                                                }
                                            }
                                        )
                                    }else{
                                        Toast.makeText(listaCar.context, "No pudimos cargar las sobras disponibles, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                                    }
                                }

                                override fun onFailure(call: Call<List<SobrasDisponibles>>, t: Throwable) {
                                    Toast.makeText(listaCar.context, "No pudimos cargar las sobras disponibles, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                                }
                            }
                        )
                    }else{
                        Toast.makeText(listaCar.context, "No pudimos cargar los tipos de sobras, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<TipoSobrante>>, t: Throwable) {
                    Toast.makeText(listaCar.context, "No pudimos cargar los tipos de sobras, por favor intenta mas tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                }
            }
        )


    }
}