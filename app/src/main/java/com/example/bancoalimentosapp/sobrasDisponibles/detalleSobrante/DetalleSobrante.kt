package com.example.bancoalimentosapp.sobrasDisponibles.detalleSobrante

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.DetalleSobranteFragmentBinding
import com.example.bancoalimentosapp.databinding.SobrasDisponiblesFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.sobrasDisponibles.SobrasDisponiblesRow
import com.example.bancoalimentosapp.sobrasDisponibles.SobrasDisponiblesViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson

class DetalleSobrante : Fragment() {

    companion object {
        fun newInstance() = DetalleSobrante()
    }

    private lateinit var viewModel: DetalleSobranteViewModel

    private var _binding: DetalleSobranteFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DetalleSobranteFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = DetalleSobranteViewModel(activity, this)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        usuario = Gson().fromJson(
            sharedPreferences!!.getString("usuario", "{}"),
            UsuarioSesion::class.java
        )
        var notificacion = arguments?.getSerializable("notificacion") as SobrasDisponiblesRow

        var nomRestDetSobrante: TextView = binding.nomRestDetSobrante
        var dirResrDetSobr: TextView = binding.dirResrDetSobr
        var telRestDetSobr: TextView = binding.telRestDetSobr
        var horaDetSobr: TextView = binding.horaDetSobr

        var detNombreSobra: TextView = binding.detNombreSobra
        var detCantSobra: TextView = binding.detCantSobra
        var detNOmbreTipoSobra: TextView = binding.detNOmbreTipoSobra

        var spinnerBancosDetSobr: Spinner = binding.spinnerBancosDetSobr
        var spinnerEmpleadosDetSobra: Spinner = binding.spinnerEmpleadosDetSobra

        var btnTomarSobrante: Button = binding.btnTomarSobrante
        var btnVOlverDetSobrante: Button = binding.btnVOlverDetSobrante

        val mapFragment = childFragmentManager.findFragmentById(R.id.mapDetSobrRest) as SupportMapFragment?

        viewModel.cargarDetalleRest(notificacion.id_restaurantes,nomRestDetSobrante,dirResrDetSobr,telRestDetSobr,horaDetSobr,mapFragment!! )
        viewModel.cargarDetalleSobrante(notificacion,detNombreSobra,detCantSobra,detNOmbreTipoSobra)
        viewModel.cargarBancos(view, spinnerBancosDetSobr, usuario.id_usuario)


        spinnerBancosDetSobr.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                vieww: View,
                position: Int,
                id: Long
            ) {
                viewModel.cargarVehiculos(vieww, spinnerEmpleadosDetSobra, viewModel.listBank[position].id_bancos_alimentos)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })

        btnTomarSobrante.setOnClickListener {

            viewModel.asignarBAncoANotificacion(spinnerBancosDetSobr, notificacion.id_notificaciones)
        }

        btnVOlverDetSobrante.setOnClickListener {
            findNavController().navigate(R.id.sobrasDisponibles)
        }
    }


}