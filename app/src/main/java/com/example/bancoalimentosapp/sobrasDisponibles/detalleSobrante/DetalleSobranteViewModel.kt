package com.example.bancoalimentosapp.sobrasDisponibles.detalleSobrante

import android.R
import android.app.Activity
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.models.AsignarBancoNotificacion
import com.example.bancoalimentosapp.models.Banco
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.models.Vehiculo
import com.example.bancoalimentosapp.services.*
import com.example.bancoalimentosapp.sobrasDisponibles.SobrasDisponiblesRow
import com.example.bancoalimentosapp.utils.Settings
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DetalleSobranteViewModel (var activity: Activity?, var fragment:Fragment ) {

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)

    val sobrasService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<SobrantesServices>(SobrantesServices::class.java)

    val tipoSobraService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<TipoSobranteService>(TipoSobranteService::class.java)

    val notificacionesService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<NotificacionesSercvice>(NotificacionesSercvice::class.java)

    val bancoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<BancoService>(BancoService::class.java)

    val vehiculoService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<VehiculoService>(VehiculoService::class.java)

    lateinit var restAux:Restaurante
    var listBank: List<Banco> = mutableListOf()
    var listVehiculos: List<Vehiculo> = mutableListOf()

    fun cargarDetalleRest(idRest:Int, nombre:TextView, direccion:TextView, telefono:TextView, hora:TextView, map:SupportMapFragment){
        restauranteService.obtenerRestaurantePorId(idRest).enqueue(
            object : Callback<Restaurante> {
                override fun onResponse(call: Call<Restaurante>, response: Response<Restaurante>) {
                    if(response.isSuccessful){
                        restAux = response.body() as Restaurante
                        nombre.text = restAux.nombre
                        direccion.text = restAux.dirección
                        telefono.text = restAux.telefono.toString()
                        hora.text = restAux.horarios
                        var geolocalizacionAux =  restAux.geolocalizacion.split(";")
                        if(!geolocalizacionAux[0].equals("-1") && geolocalizacionAux[1] != "-1"){
                            ingresarPuntoEnMapa(geolocalizacionAux,map,restAux.nombre)
                        }else{

                            ingresarPuntoEnMapa(listOf("41.889941", "12.492306"), map,"???")
                        }
                    }else{
                        Toast.makeText(nombre.context, "No pudimos cargar los datos del restaurante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<Restaurante>, t: Throwable) {
                    print("error rest : ${t.message.toString()}")
                    Toast.makeText(nombre.context, "No pudimos cargar los datos del restaurante, por favor intenta mas tarde ${t.cause}\n ${t.message}", Toast.LENGTH_LONG).show()
                }
            }
        )
    }


    fun ingresarPuntoEnMapa(punto:List<String>,mapFragment:SupportMapFragment, nombre:String){

        mapFragment!!.getMapAsync {
            val latLng = LatLng(punto[0].toDouble(),punto[1].toDouble())
            val markerOptions = MarkerOptions().position(latLng).title(nombre)
            it.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            it.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f))
            it.addMarker(markerOptions)
        }
    }

    fun cargarDetalleSobrante(sobra:SobrasDisponiblesRow,nombre:TextView,cantidad:TextView,tipo:TextView){
        nombre.text = sobra.nombre
        cantidad.text = sobra.cantidad.toString() + "Libras"
        tipo.text = sobra.tipoSobra
    }

    fun cargarVehiculos(view: View, vehiculos: Spinner, idBanco:Int){

        vehiculoService.vehiculosPorBanco(idBanco).enqueue(
            object : Callback<List<Vehiculo>> {
                override fun onResponse(
                    call: Call<List<Vehiculo>>,
                    response: Response<List<Vehiculo>>
                ) {
                    if(response.isSuccessful){
                        listVehiculos = response!!.body()!!
                        val listTitleBank: MutableList<String> = mutableListOf()
                        listVehiculos.forEach {
                            listTitleBank.add("${it.placa} - ${it.modelo}")
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            view.context,
                            R.layout.simple_spinner_item, listTitleBank
                        )

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        vehiculos.setAdapter(adapter)
                    }else{
                        Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de vehiculos", Toast.LENGTH_LONG).show()
                    }
                }
                override fun onFailure(call: Call<List<Vehiculo>>?, t: Throwable?) {
                    Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de vehiculos", Toast.LENGTH_LONG).show()
                }

            }
        )

    }

    fun cargarBancos(view: View, bancos: Spinner, idUsr:Int){

        bancoService.listarBnacosPorUsuario(idUsr).enqueue(
            object : Callback<List<Banco>> {
                override fun onResponse(
                    call: Call<List<Banco>>,
                    response: Response<List<Banco>>
                ) {
                    if(response.isSuccessful){
                        listBank = response!!.body()!!
                        val listTitleBank: MutableList<String> = mutableListOf()
                        listBank.forEach {
                            listTitleBank.add(it.nombre)
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            view.context,
                            R.layout.simple_spinner_item, listTitleBank
                        )

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        bancos.setAdapter(adapter)
                    }else{
                        Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de bancos", Toast.LENGTH_LONG).show()
                    }
                }
                override fun onFailure(call: Call<List<Banco>>?, t: Throwable?) {
                    Toast.makeText(view.context, "Error inesperado, No pudimos cargar la lista de bancos", Toast.LENGTH_LONG).show()
                }

            }
        )

    }

    fun asignarBAncoANotificacion(bancoSpinner: Spinner, idNotifi:Int){
        var idBanco = listBank.get(bancoSpinner.selectedItemPosition).id_bancos_alimentos

        if(idBanco == null){
            Toast.makeText(activity, "debe seleccionar almenos un banco que se encargara del pedido", Toast.LENGTH_LONG).show()
        }else {
            notificacionesService.asignarBAncoANotificacion(
                AsignarBancoNotificacion(idBanco, idNotifi)
            ).enqueue(
                object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        if (response.isSuccessful) {
                            fragment.findNavController().navigate(com.example.bancoalimentosapp.R.id.pedidosBanco)
                            Toast.makeText(activity, "Donante Adquirido", Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(
                                activity,
                                "NO se pudo adquirir el sobrante intenta mas tarde",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Toast.makeText(
                            activity,
                            "NO se pudo adquirir el sobrante intenta mas tarde",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            )
        }
    }

}