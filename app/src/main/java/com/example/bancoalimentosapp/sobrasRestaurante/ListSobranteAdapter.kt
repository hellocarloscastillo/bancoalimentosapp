package com.example.bancoalimentosapp.sobrasRestaurante

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.models.RowDonanteRest
import com.example.bancoalimentosapp.services.SobrantesServices
import com.example.bancoalimentosapp.sobrasDisponibles.ListSobrasDisponiblesAdapter
import com.example.bancoalimentosapp.sobrasDisponibles.SobrasDisponiblesRow
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ListSobranteAdapter (val mList: List<RowDonanteRest>): RecyclerView.Adapter<ListSobranteAdapter.ViewHolder>() {

    val sobrantesServices = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<SobrantesServices>(SobrantesServices::class.java)

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_sobrante_restaurante, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemViewModel = mList[position]

        holder.cantidadSobra.text = ItemViewModel.cantidad.toString() + " Libras"
        holder.tipoSobra.text = ItemViewModel.nombreSobrante
        holder.id.text = ItemViewModel.nombreRestaurante
        holder.idRest.text = ItemViewModel.id_restaurantes.toString()
        holder.nombre.text = ItemViewModel.nombre

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var id: TextView = itemView.findViewById(R.id.idSobranteRow)
        var idRest : TextView = itemView.findViewById(R.id.idRestDonanteRow)
        var nombre: TextView = itemView.findViewById(R.id.nombreSobranteRow)
        var cantidadSobra: TextView = itemView.findViewById(R.id.cantidadSobranteRow)
        var tipoSobra: TextView = itemView.findViewById(R.id.tipoDonanteRow)

    }

}