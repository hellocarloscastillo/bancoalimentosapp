package com.example.bancoalimentosapp.sobrasRestaurante

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.SobrasRestauranteFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson

class SobrasRestaurante : Fragment() {

    companion object {
        fun newInstance() = SobrasRestaurante()
    }

    private lateinit var viewModel: SobrasRestauranteViewModel
    private var _binding: SobrasRestauranteFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SobrasRestauranteFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val btnAddSobrante: ImageButton = binding.btnAddSobraRest
        viewModel = SobrasRestauranteViewModel(activity)

        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)

        //init list recycler view
        val recyclerListSobrantesRest = activity?.findViewById<RecyclerView>(R.id.listSobrasRest)
        recyclerListSobrantesRest?.layoutManager = LinearLayoutManager(activity)


        viewModel.listarDonantes(recyclerListSobrantesRest!!)

        btnAddSobrante.setOnClickListener {

            findNavController().navigate(R.id.registroSobrante )
        }

    }

}

