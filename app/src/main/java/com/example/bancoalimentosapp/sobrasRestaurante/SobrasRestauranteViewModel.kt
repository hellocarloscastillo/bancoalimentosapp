package com.example.bancoalimentosapp.sobrasRestaurante

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.models.RowDonanteRest
import com.example.bancoalimentosapp.services.SobrantesServices
import com.example.bancoalimentosapp.sobrasDisponibles.ListSobrasDisponiblesAdapter
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SobrasRestauranteViewModel(var activity:Activity?)  {
    val sobranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<SobrantesServices>(SobrantesServices::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    private val usuarioSession = Gson().fromJson(sharedPreferences.getString("usuario", "{}"), UsuarioSesion::class.java)


    fun listarDonantes(lista:RecyclerView){
        sobranteService.obtenerListaDonantes(usuarioSession.id_usuario).enqueue(
            object : Callback<List<RowDonanteRest>> {
                override fun onResponse(
                    call: Call<List<RowDonanteRest>>,
                    response: Response<List<RowDonanteRest>>
                ) {
                    if (response.isSuccessful) {
                        var listaResponse = response.body() as List<RowDonanteRest>
                        if(listaResponse.size > 0){
                            val adapter = ListSobranteAdapter(listaResponse )
                            lista!!.adapter = adapter
                        }else{
                            Toast.makeText(lista.context, "NO tiene donaciones registradas", Toast.LENGTH_LONG).show()
                        }
                    }
                    else{
                        Toast.makeText(lista.context, "No pudimos cargar las donaciones, por favor intenta tarde o contacta con un administrador", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<RowDonanteRest>>, t: Throwable) {
                    Toast.makeText(lista.context, "No pudimos cargar las donaciones, por favor intenta mas tarde o contacta con un administrador ${t.toString()}", Toast.LENGTH_LONG).show()
                }
            }
        )
    }

}