package com.example.bancoalimentosapp.sobrasRestaurante.registroSobrante

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.Spinner
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.RegistroSobranteFragmentBinding
import com.example.bancoalimentosapp.databinding.SobrasRestauranteFragmentBinding
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.sobrasRestaurante.SobrasRestauranteViewModel
import com.google.gson.Gson

class RegistroSobrante : Fragment() {

    companion object {
        fun newInstance() = RegistroSobrante()
    }

    private lateinit var viewModel: RegistroSobranteViewModel
    private var _binding: RegistroSobranteFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var usuario: UsuarioSesion

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RegistroSobranteFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreferences = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        val btnAddSobrante: Button = binding.btnGuardarSobrante
        val btnCancelarSobrante: Button = binding.btnCancelSobrante
        val nombre: TextView = binding.nombreSobrante
        val cantidad: TextView = binding.cantidadSobrante
        val listTipoSobrante: Spinner = binding.spinnerTipoSobrante
        val listRestaurantes: Spinner = binding.spinnerRestaurantesRobrante
        viewModel = RegistroSobranteViewModel(activity, this)

        usuario = Gson().fromJson(sharedPreferences!!.getString("usuario", "{}"), UsuarioSesion::class.java)

        viewModel.cargarTipoSobrante(listTipoSobrante)
        viewModel.cargarRestaurantes(listRestaurantes, usuario.id_usuario)


        btnAddSobrante.setOnClickListener {
            viewModel.registrarSobrante(nombre, cantidad, listTipoSobrante, listRestaurantes )
        }

        btnCancelarSobrante.setOnClickListener {

            findNavController().navigate(R.id.sobrasRestaurante)
        }

    }



}