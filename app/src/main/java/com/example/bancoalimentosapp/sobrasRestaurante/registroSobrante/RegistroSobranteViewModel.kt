package com.example.bancoalimentosapp.sobrasRestaurante.registroSobrante

import android.R
import android.app.Activity
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.models.Restaurante
import com.example.bancoalimentosapp.models.SobranteGet
import com.example.bancoalimentosapp.models.SobrantePost
import com.example.bancoalimentosapp.models.TipoSobrante
import com.example.bancoalimentosapp.services.RestauranteService
import com.example.bancoalimentosapp.services.SobrantesServices
import com.example.bancoalimentosapp.services.TipoSobranteService
import com.example.bancoalimentosapp.utils.Settings
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegistroSobranteViewModel(var activity: Activity?, var fragment:Fragment){
    val sobranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<SobrantesServices>(SobrantesServices::class.java)

    val tipoSobranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<TipoSobranteService>(TipoSobranteService::class.java)

    val restauranteService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<RestauranteService>(RestauranteService::class.java)

    var listTipoSObrante: List<TipoSobrante> = mutableListOf()
    var listRestaurantes: List<Restaurante> = mutableListOf()

    fun cargarTipoSobrante(tipoSobrantesSpinner: Spinner){
        tipoSobranteService.listarTipoRestaurante().enqueue(
            object : Callback<List<TipoSobrante>> {
                override fun onResponse(
                    call: Call<List<TipoSobrante>>,
                    response: Response<List<TipoSobrante>>
                ) {
                    if(response.isSuccessful){
                        listTipoSObrante = response!!.body()!!
                        val listTitleCar: MutableList<String> = mutableListOf()
                        listTipoSObrante.forEach {
                            listTitleCar.add("${it.nombre}")
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            activity!!,
                            R.layout.simple_spinner_item, listTitleCar
                        )

                        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
                        tipoSobrantesSpinner.setAdapter(adapter)
                    }else{
                        Toast.makeText(tipoSobrantesSpinner.context, "No se pudieron cargargar los tipos, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<TipoSobrante>>, t: Throwable) {
                    Toast.makeText(tipoSobrantesSpinner.context, "No se pudieron cargargar los tipos, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                }
            }
        )
    }

    fun cargarRestaurantes(restaurantesSobrantesSpinner:Spinner, idUsr:Int){
        restauranteService.restaurantesPorUsuario(idUsr).enqueue(
            object : Callback<List<Restaurante>> {
                override fun onResponse(
                    call: Call<List<Restaurante>>,
                    response: Response<List<Restaurante>>
                ) {
                    if(response.isSuccessful){
                        listRestaurantes = response!!.body()!!
                        val listTitleCar: MutableList<String> = mutableListOf()
                        listRestaurantes.forEach {
                            listTitleCar.add("${it.nombre}")
                        }

                        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                            activity!!,
                            R.layout.simple_spinner_item, listTitleCar
                        )

                        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
                        restaurantesSobrantesSpinner.setAdapter(adapter)
                    }else{
                        Toast.makeText(restaurantesSobrantesSpinner.context, "No se pudieron cargargar los restaurantes, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<List<Restaurante>>, t: Throwable) {
                    Toast.makeText(restaurantesSobrantesSpinner.context, "No se pudieron cargargar los restaurantes, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                }
            }
        )
    }

    fun registrarSobrante(nombre:TextView,cantidad:TextView,tipoSobrantesSpinner: Spinner, restaurantesSpinner: Spinner){

        try {
            sobranteService.guardarSobrante(validarSobrante(nombre,cantidad,tipoSobrantesSpinner, restaurantesSpinner)).enqueue(
                object : Callback<SobranteGet> {
                    override fun onResponse(
                        call: Call<SobranteGet>,
                        response: Response<SobranteGet>
                    ) {
                        Toast.makeText(nombre.context, "Donante Registrado", Toast.LENGTH_LONG).show()
                        fragment.findNavController().navigate(com.example.bancoalimentosapp.R.id.sobrasRestaurante)
                    }

                    override fun onFailure(call: Call<SobranteGet>, t: Throwable) {
                        Toast.makeText(nombre.context, "No se pudo registrar el donante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }
            )
        }catch (e: Exception){
            print(e.message)
            Toast.makeText(nombre.context, "No se pudo registrar el sobrante, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
        }

    }


    fun validarSobrante(nombre:TextView,cantidad:TextView,tipoSobrantesSpinner: Spinner, restaurantesSpinner: Spinner): SobrantePost{
        val idRest = listRestaurantes.get(restaurantesSpinner.selectedItemPosition).id_restaurantes
        val idTipo = listTipoSObrante.get(tipoSobrantesSpinner.selectedItemPosition).id_tipo_sobrante
        var sobranteAux = SobrantePost(
            if (cantidad.text.toString().equals("")) 0.0 else cantidad.text.toString().toDouble(),
            nombre.text.toString(),
            if(idTipo.equals(null)) -1 else idTipo,
            if(idRest.equals(null)) -1 else idRest)

        if(sobranteAux.cantidad < 1 ){
            throw Exception("La cantidad no puede ser 0 o nula")
        }
        if(sobranteAux.nombre.equals("") ){
            throw Exception("El nombre no puede estar vacio")
        }
        if(sobranteAux.id_tipo_sobrante < 0 ){
            throw Exception("No hay un Tipo de sobrante seleccionado")
        }

        if(sobranteAux.id_restaurantes < 0 ){
            throw Exception("No hay un restaurante seleccionado")
        }
        return sobranteAux
    }
}