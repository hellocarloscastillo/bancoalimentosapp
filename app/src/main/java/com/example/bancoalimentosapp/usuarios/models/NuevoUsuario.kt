package com.example.bancoalimentosapp.usuarios.models

data class NuevoUsuario(
    var nombre:String,
    var contrasena: String,
    var correo: String,
    var documento: Int,
    var id_tipo_usuario: Int
)
