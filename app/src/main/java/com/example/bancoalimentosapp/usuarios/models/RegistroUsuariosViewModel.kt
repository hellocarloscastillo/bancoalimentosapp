package com.example.bancoalimentosapp.usuarios.models

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.login.views.data.model.UsuarioSesion
import com.example.bancoalimentosapp.services.UsuarioService
import com.example.bancoalimentosapp.utils.Settings
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegistroUsuariosViewModel(val activity: Activity?, val fragment: Fragment)  {

    private val usuarioService = Retrofit.Builder().baseUrl(Settings.BASE_URL).addConverterFactory(
        GsonConverterFactory.create()).build()
        .create<UsuarioService>(UsuarioService::class.java)

    val sharedPreferences: SharedPreferences = activity!!.getPreferences(Context.MODE_PRIVATE)

    public fun saveUser(newUsr: NuevoUsuario, context:Context?){
        this.usuarioService.guardarUsuario(newUsr).enqueue(
                object : Callback<UsuarioSesion> {
                    override fun onResponse(
                        call: Call<UsuarioSesion>,
                        response: Response<UsuarioSesion>?
                    ) {
                        if(response!!.isSuccessful){
                            val editor:SharedPreferences.Editor =  sharedPreferences.edit()
                            val usrSession = response?.body() as UsuarioSesion
                            editor.putString("usuario", Gson().toJson(usrSession))
                            editor.apply()
                            editor.commit()
                            Toast.makeText(context, "Usuario Registrado ${usrSession.toString()}", Toast.LENGTH_LONG).show()
                            if(usrSession.tipoUsuario.id_tipo_usuario == 1 ){
                                fragment.findNavController().navigate(R.id.registroRestaurante )
                            }else {
                                fragment.findNavController().navigate(R.id.registroBanco)
                            }
                        }else{
                            Toast.makeText(context, "Lo sentimos, ocurrio un error, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<UsuarioSesion>?, t: Throwable?) {
                        Toast.makeText(context, "Lo sentimos, ocurrio un error: ${t.toString()}", Toast.LENGTH_LONG).show()
                    }
                }
        )
    }
    @Throws(Exception::class)
    fun consultarUsuarioPorCedula(documento:String, context:Context?){
        usuarioService.consultarPorDocumento(documento).enqueue(
            object : Callback<UsuarioSesion>  {
                override fun onResponse(
                    call: Call<UsuarioSesion>,
                    response: Response<UsuarioSesion>?
                ) {
                    if(response!!.isSuccessful){
                        throw Exception("ya existe un usuario con este documento")
                    }else{
                        Toast.makeText(context, "Lo sentimos, ocurrio un error, por favor intenta mas tarde", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onFailure(call: Call<UsuarioSesion>?, t: Throwable?) {
                    Toast.makeText(context, "Lo sentimos, ocurrio un error: ${t.toString()}", Toast.LENGTH_LONG).show()
                }
        })
    }
}