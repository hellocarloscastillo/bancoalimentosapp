package com.example.bancoalimentosapp.usuarios.views

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.bancoalimentosapp.R
import com.example.bancoalimentosapp.databinding.RegistroUsuariosFragmentBinding
import com.example.bancoalimentosapp.login.views.data.Result
import com.example.bancoalimentosapp.usuarios.models.NuevoUsuario
import com.example.bancoalimentosapp.usuarios.models.RegistroUsuariosViewModel
import java.lang.Exception
import java.util.*

class RegistroUsuarios : Fragment() {

    companion object {
        fun newInstance() = RegistroUsuarios()
    }

    private lateinit var viewModel: RegistroUsuariosViewModel
    private var _binding: RegistroUsuariosFragmentBinding? = null
    private val binding get() = _binding!!

    data class validateResponse(var success: Boolean, val dato: Any?)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RegistroUsuariosFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val txtNombre: TextView = binding.editName
        val txtEmail: TextView = binding.editEmail
        val txtNoDocument: TextView = binding.editNoDocument
        val txtPass: TextView = binding.editPass
        val txtConfirmPass: TextView = binding.editConfirmPass

        val btnRegister: Button = binding.btnRegister
        val btnCancel: Button = binding.btnRegresar

        viewModel = RegistroUsuariosViewModel(activity, this)

        btnRegister.setOnClickListener{
            val response = this.validateData(txtNombre,txtEmail, txtNoDocument, txtPass, txtConfirmPass)
            if( response.success){
                this.viewModel.saveUser(response.dato as NuevoUsuario, context)
            }else{
                Toast.makeText(context, response.dato as String, Toast.LENGTH_LONG).show()
            }

        }

        btnCancel.setOnClickListener {
            findNavController().navigate(R.id.loginFragment )
        }


    }

    fun validateData(nombre: TextView, email: TextView, doc: TextView, pass: TextView, confirmPass: TextView): validateResponse{
        val typeUsr: Int? = arguments?.getInt("typeUsr", -1)

        if(pass.text.toString().equals(confirmPass.text.toString())){
            val newUsr: NuevoUsuario = NuevoUsuario(nombre.text.toString(), pass.text.toString(), email.text.toString(), doc.text.toString().toInt(), typeUsr!!)
            //Toast.makeText(context, "usuario a registrar : ${newUsr}", Toast.LENGTH_LONG).show()
            if(newUsr.nombre.equals("") || newUsr.nombre.length<3){
                return validateResponse(false, "Nombre Invalido")
            }
            if(newUsr.correo.equals("") || newUsr.correo.length<3 ){
                return validateResponse(false, "correo Invalido")
            }
            if(newUsr.documento < 99999 ){
                try {
                    //viewModel.consultarUsuarioPorCedula(newUsr.documento.toString(), context)
                }
                catch (e:Exception){
                    return validateResponse(false, "alv")
                }
                return validateResponse(false, "documento Invalido")
            }
            return validateResponse(true, newUsr)
        }else{
            return validateResponse(false, "Las Contraseñas No Coinciden")
        }
    }


}