package com.example.bancoalimentosapp.utils

object Settings {
    const val BASE_URL: String = "https://project-banco-alimentos.herokuapp.com/api/"
}